![TBZ Logo](../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/m319_picto.jpg)

# Variablen und Konstanten

---

### Lernziel:
* Zweck einer Variable, einer Konstante 
* Konzept zur Variablenhandhabung: Deklarierung, Initialisierung, Verwendung
* Gültigkeitsbereich einer Variable
* Welche primitive Datentypen unterstützt JAVA

--- 

## Einführung in das Konzept von Variablen

![Video:](../x_gitressourcen/Video.png) 11:05 Min
[![Konzept](./x_gitressourcen/KV.png)](https://web.microsoftstream.com/video/ffcb99fe-5ed2-48a0-a905-7b333af14af6?channelId=1f550531-dbe5-47c1-a44f-bdf2f22b6908)

[Link zum Poster](./ProgBASICS Variablen V1.3.png)

---

# Was sind Variablen?

Jedes Computerprogramm verarbeitet Daten. Diese Daten können unterschiedlichen
Typs sein und als solche wiederum verschiedene Werte besitzen. Die Daten stellen das 'Material' dar, das ein Programm verarbeitet.

Variablen werden verwendet, um einen Wert abzuspeichern. Dieser Wert kann auch
überschrieben und somit geändert werden. Sehr oft hat eine Variable einen
Anfangswert, der zu einem späteren Zeitpunkt geändert wird (z.B. nach einer
Berechnung).

## Deklarierung, Initialisierung und Verwendung

Variablen benötigen stets einen **Datentyp** und einen **Namen**.

Beispiel, wie eine Variable in einem Programm eingeführt (**Deklarierung**) wird:

```int zahl; ```

Ich kann der Variable einen Wert zuweisen, indem ich den Wert nach einem
Gleichzeichen hinschreibe:

```zahl = 42;```

Diese erstmalige Zuweisung eines Wertes an eine Variable wird als
**Initialisierung** (oder *Definition*) bezeichnet. Sie kann zusammen mit der Deklaration oder
getrennt davon erfolgen.

Änderung des Wertes einer Variable:

```java
double preis = 120;  
preis = preis * 1.11;
```

Hier wird die Variable *preis* zuerst **deklariert** und sogleich mit dem Wert 120 **initialisiert**. Anschliessend erhält die Variable einen neuen Wert nach der Kalkulation mit 1.11.

# Gültigkeitsbereich

![Gültigkeitsbereich](./x_gitressourcen/Gueltigkeitsbereich.png)

## Was sind lokale Variablen?

Lokale Variablen werden meist zum Zwischenspeichern von Werten verwendet, etwa
als Zählvariablen in Kontrollstrukturen, oder als Variablen innerhalb von einer
Methode. Sie werden als «lokal» bezeichnet, da sie von ausserhalb der Methode
oder Kontrollstruktur nicht verwendet werden können (niht sichtbar).

Beispiel:

```java
	for (int i = 0; i < list.length; i++){
	
		System.out.println(i + 1 + ": " + list[i]);
	
	} 
```

Hier wird lokal in der for-Schleife die Variable i deklariert und sogleich mit
dem Wert 0 zugewiesen (`int i = 0;`). Die Variable wird als Zähler (=Index) verwendet, um jedes Element aus einer Liste auszugeben.

## Was sind "globale" Variablen?

"Globale" Variablen können im ganzen Programm (bei JAVA in der ganzen Klasse) aufgerufen und verwendet werden. Sie sind nicht nur in einer Funktion (oder Methode) oder Kontrollstruktur gültig. Um eine Variable "globale zu machen, muss sie direkt innerhalb des Klassenblocks deklariert werden. (Sie heissen auch Instanz- oder (mit static) Klassenvariablen).

Beispiel:

```java
class Test {

	(static) int zahl = 24;
	
	void schreibe() {
	
		System.out.println(zahl); // 24
	
	}

}
```

Hier wird die Variable **zahl** unmittelbar nach der Klassenbezeichnung deklariert und auch gleich initialisiert. Die Wertzuweisung kann auch später irgendwo im Programm erfolgen. 

> **WICHTIG!** Eine "globale" Variable wird von einer lokalen Variable im lokalen Gültigkeitsbereich **überdeckt** (= nicht mehr *direkt* ansprechbar), wenn sie den gleichen Namen hat. 

## Demo Gültigkeitsbereich:

![Video:](../x_gitressourcen/Video.png) 9:40 Min
[![Gültigkeitbereich](./x_gitressourcen/Gb.png)](https://web.microsoftstream.com/video/0d0d99c6-da76-4072-ada2-27664c634e03)

Code: [Scope.java](./scope.java)
<https://www.w3schools.com/java/java_scope.asp>

![ToDo](../x_gitressourcen/ToDo.png) Do To: [W3S Scope](https://www.w3schools.com/java/java_scope.asp)

---

# Primitive Datentypen

Je nach Datentyp hat die Variable eine andere Verwendung im Programm. Um z.Bsp.
einfache Berechnungen mit ganzen Zahlen anzustellen, wird der Datentyp
**Integer** verwendet. Um mit Wörter oder irgendwelchen Zeichenketten zu
arbeiten, werden (komplexe) **Strings** verwendet.

In Java gibt es eingebaute Datentypen (sog. **primitive Datentypen**), die man
für Datenmanipulationen verwenden kann.   
In der unteren Tabelle sind die 8 primitiven Datentypen und die zugehörige
Wrapper-Klasse aufgelistet. Neben dem Datentyp-Namen sind in der Tabelle die
Grösse des Datentyps, der Initialisierungs-Standardwert und der jeweilige
Wertebereich angegeben.

| **Datentyp** | **Größe [Bit]** | **Standardwert** | **Wertebereich (MIN MAX)** | **Wrapper-Klasse**  |
|----------|:-------------:|:----------------:|:---------------------:|----------------|
| boolean      | 1               | *false*          | *true*, *false*          | java.lang.Boolean   |
| byte         | 8               | 0                | -128 … +127              | java.lang.Byte      |
| short        | 16              | 0                | -32'768 … 32'767             | java.lang.Short     |
| int          | 32              | 0                | -2'147'483'648 … 2'147'483'647  | java.lang.Integer   |
| long         | 64              | 0                | -2^63 … 2^63-1    | java.lang.Long      |
| float        | 32              | 0.0f             | ±1.4×10^38 … ±3.4×10^38   | java.lang.Float     |
| double       | 64              | 0.0              | ±5×10^324 … ±1.8×10^308 | java.lang.Double    |
| char         | 16              | \\u0000          | \\u0000‘ … \\uFFFF‘    | java.lang.Character |

> **Anm.**: [char >> Unicode mit max. 16 Bit (UTF-16)](https://de.wikipedia.org/wiki/Unicode) <br>
> Siehe auch: ```System.out.println("Integer max: " + Integer.MAX_VALUE); ```


**Primitive vs. Komplexe Datentypen**

Zu jedem primitiven Datentyp gibt es eine sog. **Wrapper-Klasse**. Es handelt
sich dabei um eine Hilfsklasse, die man statt dem primitiven Datentyp verwenden
kann. Wrapper-Klassen bieten Methoden (Funktionen) an, um die Variable zu ändern
oder z.Bsp. als String auszugeben.

Da Java eine objektorientierte Programmiersprache ist, werden meistens
**Klassen** und ihre entsprechende Objektreferenzen benützt. ([Siehe dazu D2: Klassen und Objekte](../N2-Classes_Objects_Methods).

**String** ist ein spezieller Datentyp, da es sich hier bereits um eine Klasse
handelt (= komplexer Datentyp) und nicht um einen primitiven Datentyp. Wie bei den Wrapper-Klassen, hat String eingebaute Methoden, die man verwenden kann. ([siehe dazu D3: Komplexe Datentypen](../N3-Complex_Datatypes))

# Was ist eine Konstante?

Konstanten sind ähnlich wie Variablen; mit dem Unterschied, dass der Wert
**nicht mehr verändert werden kann** (= er bleibt konstant). Die Konstante muss
auch sofort mit einem Wert versehen (initialisiert) werden. Üblicherweise werden Konstanzen **GROSS** geschrieben, mit "_" als Trenner von Worten:

Die Konstante wird mit dem Schlüsselwort **final** bezeichnet:

```java
final char CONST_C = ‘c’;
```

Konstanten machen dann Sinn, wenn sich der Wert nicht ändert, z.B. bei der Zahl *pi* oder bei einem Umrechnungskurs (den man einmal im Programm festlegt).


## Quellen und weitere Erklärungen:

[![Buch](../x_gitressourcen/Buch.jpg)Unterlagen Kap. 2.2, 2.3 A.15.2](../Knowledge_Library/Java_Programmieren.pdf)

<https://javabeginners.de/Grundlagen/Variablen.php>

<https://de.wikibooks.org/wiki/Java_Standard:_Primitive_Datentypen>

<https://www.programmierenlernenhq.de/java-grundlagen-die-2-datentypenarten-in-java-und-ihre-verwendung/>

<https://www.w3schools.com/java/java_wrapper_classes.asp>

<https://www.java-tutorial.org/datentypenundvariablen.html>

---

# Checkpoint
* Konzept der Variabeln (Konstanten) ist bekannt. <br> >> Behälter mit Name, Datentyp, Grösse und (fixem) Inhalt.
* Erkenne Deklarierung, Initialisierung und Verwendung von Variablen (Konstanten).
* Kann den Gültigkeitbereich "setzen". (Platzierung der Deklarierung im Code!).
* Wann wird eine "globale" Variable übderdeckt?



