![TBZ Logo](../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/m319_picto.jpg)

# UML Activity Diagramm

---

### Lernziel:
* Was UML?
* Was ist AD?
* Bedeutung der Symbole

--- 

## Einführung

Die **Unified Modeling Language** (vereinheitlichte Modellierungssprache), kurz **UML**, ist eine grafische Modellierungssprache zur Spezifikation, Konstruktion, Dokumentation und Visualisierung von Software-Teilen und anderen Systemen. Sie besteht aus verschiedenen Diagrammtypen: *Aktivitätsdiagramm, Anwendungsfallsdiagramm, Klassendiagramm, Sequenzdiagramm* und weitere ...

![UML](./x_gitressourcen/UML.png) [Wikipedia](https://de.wikipedia.org/wiki/Unified_Modeling_Language#Aktivit%C3%A4ten)

Ein **Aktivitätsdiagramm** (AD) ist ein Modell für ein Verhalten. Sie besteht aus Aktionen, zwischen denen Kontroll- und Datenflüsse existieren. Ein AD stellt das **dynamische Verhalten** eines Software-Systems (Programms) dar. 
AD werden für *Prozesse*, *Handlungen*, *Algorithmen* und auch *Anwendungsfälle* verwendet. Sie können in Anforderungslisten die Funktion eines Programms darstellen.

### Die wichtigsten Symbole

| Symbol | Bedeutung | Anwendung |
|:---:|---|---|
|![Start Stop](./x_gitressourcen/StartStop.png)|Start- und Endknoten|Zeigt den Beginn und das Ende des Ablaufs|
|![Aktivität](./x_gitressourcen/Activity.png)|Aktivität|Zeigt eine Aktivität und/oder Aktion an. Diese Symbole enthalten kurze Beschreibungen, die direkt in die jeweilige Form eingefügt werden, und bilden die Hauptbausteine eines Aktivitätsdiagramms. (Nomen & Verb)|
|![Fluss](./x_gitressourcen/Flow.png)|Kontrollfluss|Pfeile zeigen den Kontrollfluss und führen von einem Element zum nächsten.|
|![Verzeigung](./x_gitressourcen/Merge.png)|Entscheidung, Verzweigung, Zusammenführung|Zeigt den Kontrollfluss bei einer Entscheidung oder Rückführung einer Schleife. Zusammenführungen des Kontrollflusses müssen zwingend mit einer Raute erfolgen. Es wird nicht auf die anderen Inputs gewartet.|
|![Call](./x_gitressourcen/Call Activity.png)|Aktivitäts-Aufruf|Zeigt, dass eine separate Aktivität aufgerufen wird. Somit kann eine Aktivität eine andere aufrufen.|
|![Fork](./x_gitressourcen/Fork.png)|Teilung, paralleler Kontrollfluss|Teilung initiiert parallele Prozesse, die gleichzeitig ablaufen|
|![Join](./x_gitressourcen/Join.png)|Synchronisation|Synchronisation zeigt die Zusammenführung. Der nächste Schritt ist nur möglich, wenn beide Prozesse abgeschlossen sind. |
|![Data](./x_gitressourcen/Data.png)|Daten Elemente|Bezeichnet beim Datenfluss bearbeitete Daten. Rechteck kann auch an der Aktivität angebunden sein. (Siehe Complexe ADs &#8594; A3)|


Hier ist ein Beispiel eines AD. Der Use Case ist das Einchecken am Flughafen und Betreten eines Flugzeugs. <br> *Beachte: Der Startpunkt ist hier ein externes Ereignis.*

![AD Beispiel](./x_gitressourcen/AD_Bsp.jpg)

### Erklärung:

**Startknoten und Endknoten verwenden**<br>
Im obigen Beispiel wird ein externes Ereignis als Startknoten verwendet. Wichtig ist, dass jeder Ablauf einen Start und ein Ende hat.

**Verzeigung** <br>
Verzeigungen führen entweder Kontrollflüsse (unbedingt) zusammen oder verzweigen bedingt. Die Bedingung wird beim Kontrollfluss-Pfeil angegeben: `[Bedingung]`.

**Teilungen und Synchronisation** <br>
Teilungen (mit dem schwarzen Balken) zeigen auf, dass gleichzeitig zwei Abläufe geschehen (im obigen Beispiel: der Passagier betritt das Flugzeug, während das Gepäck verladen wird). Wichtig ist, dass eine Teilung stets am Schluss wieder zusammen kommt (Synchronisation).  Erst dann geht der Ablauf weiter.

**Synchronisation** <br>
Wenn parallele Prozesse wieder zusammengeführt werden, dann darf es nur einen Ausgang (d.h. einen nächsten Schritt) geben. 


Es gibt noch weitere Elemente in einem AD. So kann man z.B. auch Benachrichtigungen darstellen (Signale senden und empfangen). Man kann auch einen zeitlichen Trigger darstellen (wenn z.Bsp. eine Aktion zu einem bestimmten Zeitpunkt ausgelöst wird). Es kann ein Datenfluss im Kontrollfluss integriert werden. (Siehe Complexe ADs &#8594; A3)

Hier gibt’s mehr Details, u.a. auch zur Darstellung von verschachtelten Abläufen:

<https://sourcemaking.com/uml/modeling-business-systems/external-view/activity-diagrams>

<https://de.wikipedia.org/wiki/Aktivit%C3%A4tsdiagramm>

### Cheat Sheet AD:
[UML V2.5 - AD Übersicht](./Notationsuebersicht UML 2.5 3.pdf)


---

![ToDo](../x_gitressourcen/ToDo.png) Do To:

## Wahl eines UML-Tools:

Werkzeuge, um Aktivitätsdiagramme darzustellen:

		
**DrawIO** (Recommended UML / UML 2.5): <https://www.draw.io/>

**StarUML** (Free ProfiTool WIN/mac): <http://staruml.io/>

**Modelio** (Free Profitool WIN) : <https://www.modelio.org/>

**UMLet** (UMLetino online): <https://www.umlet.com/>

**Violet** (Generates html diagrams, needs Java 8): <http://alexdp.free.fr/violetumleditor/page.php>

Oder natürlich auch **MS Visio** ([>>> TBZ EDU](https://portal.azure.com/#blade/Microsoft_Azure_Education/EducationMenuBlade/software)) oder andere Graphikprogramme.

---

## Einführung AD mit StarUML

![Video:](../x_gitressourcen/Video.png)
[![Einführung](./x_gitressourcen/StarUML.png)](https://web.microsoftstream.com/video/61f4cc4b-f420-4d6b-b072-88bfab229a43)

[Doku StarUML AD](http://staruml.sourceforge.net/docs/user-guide(en)/ch05_6.html)

---

# Checkpoint
* Kenne die Symbole von AD
* Habe ein UML-Tool installiert und kann ADs erstellen ...



