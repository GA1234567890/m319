![TBZ Logo](../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/m319_picto.jpg)

# Programmier Tipps

---

### Lernziel:
* Allg. Programmiertipps
* Einführung in weitere komplexe Datentypen 
* Einführung Kommandozeilenparameter
* Einführung in Datei-Handling

---

![Erklärung](../x_gitressourcen/UC.jpg)

babla

# Debugging mit bedingter Terminalausgabe

Es ist eine gängige Art beim Debuggen einzelnen Variableninhalte auf das Termial auszugeben. Nachteil ist, dass diese PrintLn-Befehle oft vergessen gehen und im finalen Programm zurück bleiben. Mit dem einfachen Trick lassen sich die `println`'s global ein- bzw. ausschalten:

`boolean dbgPrintout = true` oder `false;` steuert die Ausgabe.

`if (dbgPrintout) System.out.println("Zeit : Ort : Var = Inhalt...`  muss hinzugefühgt werden. 


Beispiel:

```
public class DebugOnOff {
    
    boolean dbgPrintout = true; // set debug  print out globally

    public static void main(String[] args ) {

        int var = 2;

        if (dbgPrintout) System.out.println("Im Main: var=" + var);

        // Calls the funtion "func"
        func();

        if (dbgPrintout) System.out.println("Im Main: var=" + var);
    }

    // function which runs under main
    public static void func() {

        int var = 3;
        
        if (dbgPrintout) System.out.println("In Func: var=" + var);
    }

}
```

**Für Profis**: Man könnte hier auch die Hilsklasse [`Logger`](https://docs.oracle.com/en/java/javase/16/docs/api/java.logging/java/util/logging/Logger.html) verwenden: [Beispiel](https://stackoverflow.com/questions/15758685/how-to-write-logs-in-text-file-when-using-java-util-logging-logger/15758768)

-

# Gebrochene Zahlen

**Achtung** beim Vergleichen von `float`- und `double`-Zahlen: 

[![Buch](../x_gitressourcen/Buch.jpg)Unterlagen Kap. 3.6](../Knowledge_Library/Java_Programmieren.pdf)

-
# Funktionen robust machen

Bei Funktionen mit Parameterübergabe Wächter einbauen: 

[![Buch](../x_gitressourcen/Buch.jpg)Unterlagen Kap. 5.6](../Knowledge_Library/Java_Programmieren.pdf)


-

# Merker (Flags)



-
# Bessere Logik

Bei der Wahl der Bedingungen kann optimiert werden:
[![Buch](../x_gitressourcen/Buch.jpg)Unterlagen Kap. 3.7, 3.8](../Knowledge_Library/Java_Programmieren.pdf)
-

# Bitmaskierung (Mengen)

[![Buch](../x_gitressourcen/Buch.jpg)Unterlagen Anhang A.16](../Knowledge_Library/Java_Programmieren.pdf)
-


# Enums (Aufzählung)

Verwenden von sprechenden Konstanten anstelle von Zahlen:

[![Buch](../x_gitressourcen/Buch.jpg)Unterlagen Anhang A.15](../Knowledge_Library/Java_Programmieren.pdf)

[W3S Enum](https://www.w3schools.com/java/java_enums.asp)

*Für Profis:* [Buch Java ist auch eine Insel: enums](https://openbook.rheinwerk-verlag.de/javainsel/09_006.html#u9.6)

-
# Datei Handling 
blabla

[![Buch](../x_gitressourcen/Buch.jpg)Unterlagen Anhang A.20](../Knowledge_Library/Java_Programmieren.pdf)

[W3S Files](https://www.w3schools.com/java/java_files.asp)

[W3S Creaste Files](https://www.w3schools.com/java/java_files_create.asp)

[W3S Read Files](https://www.w3schools.com/java/java_files_read.asp)

[W3S Delete Files](https://www.w3schools.com/java/java_files_delete.asp)

-

# Date and Time 

[W3S Date and Time](https://www.w3schools.com/java/java_date.asp)


# Collection Framework 
blabla

[![Buch](../x_gitressourcen/Buch.jpg)Unterlagen Anhang A.21](../Knowledge_Library/Java_Programmieren.pdf)
-
# Kommandozeileneingabe 
blabla

[![Buch](../x_gitressourcen/Buch.jpg)Unterlagen Anhang A.23](../Knowledge_Library/Java_Programmieren.pdf)


