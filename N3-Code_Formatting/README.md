![TBZ Logo](../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/m319_picto.jpg)

# Code Formatierung

---

### Lernziel:
* Was ist eine "Code Convention"? 
* Quelltext Konvention TBZ-IT
* Quelltext Formatierung mit IntelliJ

---

# Von "Clean Code" und "Code Convention"

Einführung "Clean Code": (ab 2:20)

![Video:](../x_gitressourcen/Video.png) 11min [![Tutorial](https://img.youtube.com/vi/UjhX2sVf0eg/0.jpg)](https://www.youtube.com/watch?v=UjhX2sVf0eg)

Die folgende Seite befasst sich mit dem Thema "Clean Code":
[Professionalität in der SW-Entwicklung](https://clean-code-developer.de/)

Für uns ist wohl die Quelltextformatierung (Code Conventions) ein erster Schritt in die richtige Richtung: [Quelltextformatierung](https://de.wikipedia.org/wiki/Quelltextformatierung)

---
---

## Quelltext Konvention TBZ-IT

Wir wollen uns in unseren Projekten an folgende Konvention halten:

> *	Sauberes, einheitliches **Einrücken** der Kontrollstrukturen (mind. 4 Zeichen)
> *	Saubere **Abstände** bei Ausdrücken und Berechnungen (Leerschlag)
> *	Variablennamen **selbstsprechend** (*kein* x, var, temp)
>    - **boolean** Variablen &#8594; "ist" oder "is" voranstellen: **ist**Hungrig
>    - *Optional Variablennamen mit primitiven Datentypen: Typenkürzel voranstellen (**s**Name, **d**SeiteA, **i**Anzahl, ...)*
> 
> *	Einheitliche Gross- / Kleinschreibweise: 
>    - **Variabeln** und **Methoden** beginnen **k**lein, 
>    - **Klassen** beginnen **G**ross 
>    - und Rest des Bezeichners in **CamelCase**. 
>   - **Konstanten** alle Buchstaben **GROSS_CONST** mit Underscore.

> *	Programme und Funktionen werden mit "**Header**" versehen (Zweck, Input, Output &#8594; JavaDoc)
> *	Abschnittskommentare sind "**Überschriften** zum Algorithmus"
> *	Spezifischer Linien-Kommentar ist die Antwort auf "**WOZU?**"
> 
> *	**Einheitliche Sprache** (z.B. Code und Variablen &#8594; English / Kommentar &#8594; Deutsch)

---
---

Wenn's interessiert: [Google Code Convention Java](https://google.github.io/styleguide/javaguide.html)

---

# Quelltext Formatierung mit IntelliJ

Die zwei wichtigen Menüpunkte zur Formatierung unseres Quelltextes sind **>Code** und **>Refator**. Im folgenden werden die für uns **wichtigen Unterpunkte** erklärt:

## >Code
In diesem Menü kann ihr Quelltext *analysiert*, *aufgeräumt* und *ergänzt* werden. Es gelten die internen Vorgaben (default) oder individuell gesetzte:

### >Code>Code Cleanup ...
Der Quelltext wird untersucht und wenn möglich angepasst.

### >Code>Comment with ...
Entsprechender Kommentar wird vorbereitet.

### >Code>Reformat file  ...
Die einzelnen Formatierungsmöglichkeiten werden hier für die gesamte Datei zusammengefasst:
![Reformat file](./x_gitressourcen/Reformat file.png)

### >Code>Move Line up/down
Verschiebt Linie nach oben oder unten.

## >Refactor

"Refactoring" bedeutet *Umstrukturierung*, *Überarbeitung*, *Umgestaltung*. In dieses Menü die finden Sie  alle Möglihkeiten die IntelliJ dazu bietet. Im folgenden werden die für uns wichtigen Unterpunkte erklärt:

### > Refactor>Refactor this ... (CTRL-T)
Es werden Vorschläge zum Ändern des markierten Textes gegeben! 

### > Refactor>Change Signature ... 
**Erklärung**: "*Signatur*" ist der Deklarationsteil einer Klasse oder Methode (mit Parameter). <br> Damit wird im gesamten Code die Methode angepasst, inkl. aller Aufrufe!

### > Refactor>Move Members ...
**Erklärung**: "*Members*" sind die Elemente der Klasse: alle Variablen(-Arten), all eMethoden(-Arten). <br> Damit kann im Code ein Element dach oben oder unten verschoben werden!


---

![ToDo](../x_gitressourcen/ToDo.png) Do To:
Testen Sie die obigen Möglcihkeiten an ihrem Quelltext aus!

---

# Checkpoint
* Kenne den Nutzen einer "Code Convention"
* Kenne die "Quelltext Konvention TBZ-IT" und weiss sie umzusetzen!
* Kann in der IDE die Menüpunkte von **>Code** und **>Refactor** produktiv einsetzen &#8594; LERNPROZESS


