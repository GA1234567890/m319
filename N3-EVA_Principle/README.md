![TBZ Logo](../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/m319_picto.jpg)

# Das EVA-Prinzip

---

### Lernziel:
* Funktionen mit Parameter und Rückgabewerte 
* Das EVA-Prinzip für einfache Programme


---

# Unterprogramme mit Rückgabewert

![ToDo](../x_gitressourcen/ToDo.png) Do To: Studieren Sie folgende Kapitel:

[![Buch](../x_gitressourcen/Buch.jpg)Unterlagen Kap.5.2.2 bis 5.2.5](../Knowledge_Library/Java_Programmieren.pdf)

Beachten Sie die Darstellung des Unterprogramms im Activity Diagramm!

[W3S Parameter](https://www.w3schools.com/java/java_methods_param.asp)

---

# Das EVA-Prinzip

Einfache Programme sollten zwecks Übersichtlichkeit und Wartbarkeit gemäss dem sog. EVA-Prinzip programmiert werden. Programmierte Funktionen greifen auf (eingegebene) Daten zu, verarbeiten diese sinnvoll gemäss Aufgabenstellung und geben die Resultate (Daten) wieder aus.

Das EVA-Prinzip beschreibt ein Grundprinzip der Datenverarbeitung. Die Abkürzung leitet sich aus den ersten Buchstaben der Begriffe **E**ingabe, **V**erarbeitung und **A**usgabe ab.
Diese drei Begriffe beschreiben die Reihenfolge, in der Daten verarbeitet werden:

![EVA](./x_gitressourcen/EVA.png)


## Schlechte Umsetzung

Hier wird "EVA" **durchmischt**. Eingabe und Ausgabe kommen überall vor und sind nicht gruppiert: AEVAVA. <br> Das Programm ist dadurch schwer erweiter- oder wartbar.


```Java
 public static void main(String[] args ) {

        // Title
        System.out.println("Dreiecksberechnung:");
        out.println();

        // Input
        double a = inputDouble("Geben Sie die Seite a ein: ");
        double b = inputDouble("Geben Sie die Seite b ein: ");

        // Calculation
        double c = Math.sqrt(Math.pow(a,2) + pow(b,2)); // Pythagoras

        // System.Out function (Short version)
        out.println("Das Resultat ist: " + c);

        /* a random number generation */
        Random random = new Random();

        // Calling the nextInt() method
        System.out.println("Hier noch eine zufällige Ganzzahl: " + random.nextInt());
 }
 
```

## Bessere Umsetzung

*EVA ohne Funktionen*: Eingabe-, Verarbeitungs- und Ausgabeefehle sind strickt **gruppiert**. Die Ausgabe wird in einer Variable vorbereitet und an **einer Stelle ausgegeben**.

**Vorteile:**

* Themen sind gruppiert, d.h zusammengefasst.
* Eingabe könnte auch von einer Datei eingelesen werden.
* Ausgabe an einer Stelle. Ausgabe auf Drucker oder File ist leicht zu integrieren.
* Verarbeitung ist zusammengefasst.  

```
        // Variable list:
        String ausgabe; // Ausgabebuffer

        // Input
        double a = inputDouble("Geben Sie die Seite a ein: ");
        double b = inputDouble("Geben Sie die Seite b ein: ");

        // Calculation
        double c = Math.sqrt(Math.pow(a,2) + pow(b,2)); // Pythagoras
        /* a random number generation */
        Random random = new Random();
        int r = random.nextInt();

        // Output
        ausgabe = "\nDreiecksberechnung:\n";
        ausgabe = ausgabe + "\nDie Seite c ist: " + c;
        ausgabe = ausgabe + "\nHier noch eine zufällige Ganzzahl: " + r;

        System.out.println(ausgabe); // Concrete output
    }
```

## Gute Umsetzung

*EVA mit Funktionen*: Eingabe-, Verarbeitungs- und Ausgabeefehle sind in je eine Funktion ausgelagert. Alle Variablen mit den relevanten Daten sind an einer Stelle deklariert.

**Vorteile:**

* Themen sind zusammengefasst und **abstrahiert** ( &#8594; Funktionsname).
* **Variablenliste** gibt Überblick über alle *Prozess-Daten*. (Hier keine lokalen Variablen aufgeführt)
* Im `main` ist die grobe **Struktur** des Programms **sichtbar** (=EVA) gemacht.  



```java
public class Main {

    // Global variable list:
    static String ausgabe;  // Ausgabebuffer
    static double a;        // Seite a
    static double b;        // Seite b
    static double c;        // Seite c
    static int r;           // Zufallszahl r

    private static void  input() {
        // Input function
        a = inputDouble("Geben Sie die Seite a ein: ");
        b = inputDouble("Geben Sie die Seite b ein: ");
    }

    private static void  calc() {
        // Calculation function
        c = Math.sqrt(Math.pow(a,2) + pow(b,2)); // Pythagoras
        /* a random number generation */
        Random random = new Random();
        r = random.nextInt();
    }

    private static void  output() {
        // Output function
        ausgabe = "\nDreiecksberechnung:\n";
        ausgabe = ausgabe + "\nDie Seite c ist: " + c;
        ausgabe = ausgabe + "\nHier noch eine zufällige Ganzzahl: " + r;

        System.out.println(ausgabe); // Concrete output
    }
    
    public static void main(String[] args ) {
        // Only function calls
        input();
        calc();
        output();
    }
}

```

**Anm**.: Eine noch bessere Umsetzung wäre, das EVA-Prinzip mit Klassen zu realisieren. (Siehe nächstes Modul).


---

# Checkpoint
* Was beduetet EVA?
* Wie wird das EVA-Prinzip umgesetzt?
* Wie werden Funktionen eingesetzt?


