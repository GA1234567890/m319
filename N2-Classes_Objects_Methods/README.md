![TBZ Logo](../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/m319_picto.jpg)

# Klassen, Objekt und Methoden

---

### Lernziel:
* Was ist eine Klasse?
* Was ist ein Objekt, eine Instanz?
* Was ist eine Instanzvariable?
* Was ist eine (Objekt-)Methode?
* Was ist eine Klassenvariable?
* Was ist eine statische Methode?

---


# Von Bauplan und Häusern ...

Wenn ein Wohnquartier mit gleichen Häusern geplant wird, zeichnet der Architekt **einen Bauplan** für alle Häuser. Dabei versieht er alle Häuser mit **derselben Funktionalität**, lässt aber beim **Aussehen individuelle** Wünsche zu. Eine Baufirma setzt dann den Bauplan um und erstellt **mehrere** Häuser.

![Bauplan_Haus](./x_gitressourcen/Bauplan_Haus.jpg)

Es gibt bei diesem Bauvorhaben auch **Kenndaten**, die für alle Häuser **gemeinsam** gelten. Auch werden **Dienste** für alle Häuser **gemeinsam** zur Verfügung gestellt.

# ... zu Klasse und Objekten

Obiges Beispiel lässt sich auf Klassen und Objekte abbilden:

1. Eine Klasse ist der Bauplan (=Code) für **Objekte der gleichen Art** (=Objekttyp * ). Ein Objekt wird auch **Instanz** genannt!
2. Eine Klasse definiert **Instanzvariablen**, die in jedem Objekt angelegt, *individuell* initialisiert und verwendet werden. Sie sind innerhalb des Objektes *"global"*. Üblicherweise sind Instanzvariablen *private*, d.h nur innerhalb des Objektes sichtbar.
3. Eine Klasse definiert (Objekt-)**Methoden**, die dann in jedem Objekt ausgeführt werden können. Eine solche Methode bezieht sich dabei immer auf das *eigene* Objekt, in welchem sie ausgeführt wird (`this.`). 
4. Eine Klasse definiert *spezielle Methoden*, die nur zur Initialisierung der Objekte bei der Erzeugung (`new`) dienen: sog. **Konstruktoren** <br><br>
![Klasse_Objekt](./x_gitressourcen/Klasse_Objekt.jpg)
<br><br>
5. Klassen können auch Variablen definieren, die es nur einmal (`static`) gibt, und von allen Objekten benutzt werden können: sog. **Klassenvariablen**.
6. Klassen können auch Methoden definieren, die es nur einmal (`static`) gibt, und von allen Objekten benutzt werden können: sog. **statische (Klassen-) Methoden**. Üblicherweise wird hiermit ein Programm gestartet. Statische Methoden können einfach auch als **Funktionen** bezeichnet werden.

> **WICHTIG**: (*) Die Klasse ist auch ein Typ, d.h. man kann Objektvariablen von Typ der Klasse erstellen. Der Typ heisst gleich wie die Klasse selber.

![Video:](../x_gitressourcen/Video.png) Erklärung: 16 min
[![Erklärung](./x_gitressourcen/KOM.png)](https://web.microsoftstream.com/video/698bd6a5-0971-40c9-8bf3-99a20b50ed83)

---

# Konkrete Klassen und deren Methoden

In diesem Modul werden wir keine Klassen selber designen, sondern Klassen entweder als (notwendiges) **Gerüst** für die Methode `Main()` verwenden, oder Klassen der **JAVA-Bibliothek** benutzen.

## Methoden: Definition und Aufruf
Methoden ermöglichen uns, eine Abstraktion in JAVA zu programmieren. 

([Rückblick Abstraktion](../N1-Flow_Control/README.md#Abstraktion))

Wir unterscheiden zwei Code-Bereiche:

**1. Definition der Methode** innerhalb einer Klasse

```java
public class Input {
    ...
    public static String inputString(String frage) {
        ...
    }
```

**2. Aufruf der Methode** mit dem Punktoperator <br> \<returnVar> = **Klasse**.methode(\<parameter>);  // Funktionsaufruf aus einer bestimmten Klasse (-> Import) <br>  *oder* <br> \<returnVar> = **obj**.methode(\<parameter>); // Methodenaufruf eines Objektes   <br>  *oder* <br> \<returnVar> = **this**.methode(\<parameter>); // Methodenaufruf im eigenen Objekt (this. kann weggelassen werden)

```java
    String str = Input.inputString();
```


[![Buch](../x_gitressourcen/Buch.jpg)Unterlagen Anhang A.10 Seite 148](../Knowledge_Library/Java_Programmieren.pdf)

---

![ToDo](../x_gitressourcen/ToDo.png) Do To: Studieren Sie folgende Klassen ...

## Beispiel Template "Main.java"

![Main](./x_gitressourcen/Main.jpg)

1. **Klasse**: `Main.java` Dient als Gerüst für unsere Programme ...
2. **Instanzvariablen**: keine
3. **Objektmethoden**: keine
4. **Konstruktoren**: keine
5. **Klassenvariablen**: *"globale" Variablen* für die Methode `Main()`.
6. **Klassenmethoden**: `public static void main()` und weitere ...

## Beispiel Bibliotheksklasse "Math"

![Math](./x_gitressourcen/Math.jpg)

1. **Klasse**: `java.lang.Math` stellt mathematische Funktionen bereit 
2. **Instanzvariablen**: keine
3. **Objektmethoden**: keine
4. **Konstruktoren**: keine
5. **Klassenvariablen**: PI und E
6. **Klassenmethoden**: diverse mathematische Funktionen ([siehe API: java.lang.Math](https://docs.oracle.com/en/java/javase/16/docs/api/java.base/java/lang/Math.html))

Beispiel Triangle:

```java
    double a = 4.0;
    double b = 3.0;

    // Calculation using the Math Library
    double c = Math.sqrt( Math.pow(a,2) + pow(b,2) ); // Pythagoras

    System.out.println("Das Resultat ist: " + c);

```
[W3S Math Methods](https://www.w3schools.com/java/java_ref_math.asp)

[Für Profis: Weitere Infos ...](https://openbook.rheinwerk-verlag.de/javainsel/22_003.html#i22_54)

## Beispiel Wrapper-Klasse "Integer"

![Math](./x_gitressourcen/Integer.jpg)

1. **Klasse**: `java.lang.Integer` Wrapper-Klasse für den primitiven Datentyp **Integer**
2. **Instanzvariablen**: keine
3. **Objektmethoden**: Methoden für die gewrappten Objekte, z.B. `charValue()`
4. **Konstruktoren**: `Integer( i )`, kann aber weggelassen werden!
5. **Klassenvariablen**: diverse, z.B. `MAX_VALUE`
6. **Klassenmethoden**: diverse Test- und Umwandlungs-Funktionen ([siehe API: java.lang.Integer](https://docs.oracle.com/en/java/javase/16/docs/api/java.base/java/lang/Integer.html))

Beispiel UseWrapper:

```java
	//use primitive type
	int myNumber = 88;
	
	//convert into a wrapper class
	Integer wrappedNumber = Integer.valueOf(myNumber);
	
	//change to string:
	String stringValue = wrappedNumber.toString();
```
[Für Profis: Weitere Infos ...](https://openbook.rheinwerk-verlag.de/javainsel/09_004.html#i09_70)

## Beispiel Wrapper-Klasse "Character"

![Math](./x_gitressourcen/Character.jpg)

1. **Klasse**: `java.lang.Character` Wrapper-Klasse für den primitiven Datentyp **char**
2. **Instanzvariablen**: keine
3. **Objektmethoden**: Methoden für die gewrappten Objekte, z.B. `charValue()`
4. **Konstruktoren**: `Character( c )`, kann aber weggelassen werden!
5. **Klassenvariablen**: diverse, z.B. `MAX_VALUE`
6. **Klassenmethoden**: diverse Test- und Umwandlungs-Funktionen ([siehe API: java.lang.Character](https://docs.oracle.com/en/java/javase/16/docs/api/java.base/java/lang/Character.html))

Beispiel UppercaseWriter:

```java
  public static void main( String[] args )
  {
    String input = InputString();

    for ( int i = 0; i < input.length(); i++ )
    {
      char c = input.charAt( i );
      if ( Character.isWhitespace( c ) ) {
          System.out.print( '_' );
      } else if ( Character.isLetter( c ) ) {
          System.out.print( Character.toUpperCase( c ) );
      }  
    }
  }
```
[Für Profis: Weitere Infos ...](https://openbook.rheinwerk-verlag.de/javainsel/04_002.html#u4.2)

## Beispiel Helper-Klasse "Scanner"

Hier haben wir ein Beispiel einer Klasse, die ein Objekt modelliert, welches viele Methoden bereitstellt:

![Math](./x_gitressourcen/Scanner.jpg)

1. **Klasse**: `java.util.Scanner` Scannt Text nach betimmtem Inhalt durch
2. **Instanzvariablen**: keine
3. **Objektmethoden**: diverse Methoden des Scanners ([siehe API: java.util.Scanner](https://docs.oracle.com/en/java/javase/16/docs/api/java.base/java/util/Scanner.html))
4. **Konstruktoren**: Ein Scannerobjekt muss/kann für verschiedene Eingabekanäle oder -variablen erstellt werden!
5. **Klassenvariablen**: keine
6. **Klassenmethoden**: keine 

[![Buch](../x_gitressourcen/Buch.jpg)Unterlagen Seite 140..143](../Knowledge_Library/Java_Programmieren.pdf)

Beispiel Scanner:

```java
	// Creates an object scanner for the input string
	Scanner scanner = new Scanner( "tutego 12 1973 12,03 True 123456789000" );
	
	// Scans through the string
	System.out.println( scanner.hasNext() );        // true
	System.out.println( scanner.next() );           // tutego
	System.out.println( scanner.hasNextByte() );    // true
	System.out.println( scanner.nextByte() );       // 12
	System.out.println( scanner.hasNextInt() );     // true
	System.out.println( scanner.nextInt() );        // 1973
	System.out.println( scanner.hasNextDouble() );  // true
	System.out.println( scanner.nextDouble() );     // 12.03
	System.out.println( scanner.hasNextBoolean() ); // true
	System.out.println( scanner.nextBoolean() );    // true
	System.out.println( scanner.hasNextLong() );    // true
	System.out.println( scanner.nextLong() );       // 123456789000
	System.out.println( scanner.hasNext() );        // false
```

[W3S Scanner User Input](https://www.w3schools.com/java/java_user_input.asp)

[Für Profis: Weitere Infos ...](https://openbook.rheinwerk-verlag.de/javainsel/04_009.html#i04_126)

## Weitere Klassen ...

Es gibt Klasse, die stellen **komplexe Datentypen** zur Verfügung: <br>
z.B. **String** und **Arraylist** ([siehe komplexe Datentypen &#8594; D3](../N3-Complex_Datatypes))

---

# Checkpoint
* **Wozu** dient eine Klasse?
* Was ist der Unterschied zwischen Objekt und Instanz?
* Was ist der Unterschied zwischen Klassen- und Instanzvariablen?
* Was ist der Unterschied zwischen statischen und *normalen* Methoden?
* Wie wird eine Methode **definiert**?
* Wie wird eine Methode **aufgerufen**?
* Wie heisst der **Typ einer Klasse**?
* Was ist eine Funktion?
* JAVA API verwenden können, um Methoden zu finden.


