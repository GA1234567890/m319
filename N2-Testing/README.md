![TBZ Logo](../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/m319_picto.jpg)

# Testing

---

### Lernziel:
* Welche **Fehlerarten** gibt es bei der SW-Entwicklung.
* Weiss wo ich die Standards (IEEE) einsehen kann.
* Was ist ein **Testverfahren**.
* Die zwei Testmethoden: **Black-Box-** und **White-Box-Tests**
* Was ist ein **Code-Review** und wie wird er angewendet.

---

# Ein Überblick:

Die IDE hilft uns **Syntaxfehler** zu erkennen (&#8594; Fehlermeldungen). Alle weiteren Fehlerarten müssen durch systematische Verfahren vermieden oder eliminiert werden:

* **Laufzeitfehler** (z.B. Bedingungen für die Ausführung nicht gegeben &#8594; angegebene Datei fehlt) <br>
  Führen zum _Absturz des Programms_, da der Fehler im Prgramm nicht berücksichtigt wurde!
* **Codierfehler** (z.B. bei Zeichencodierung, bei Datentypumwandlung)
* **Logikfehler** (z.B. bei Entscheidungen)
* **Strukturfehler** (z.B. bei Iterationen)
* **Entwurfsfehler** (z.B. Realität falsch in der SW abgebildet)
* **Datenfehler** (z. B. char/integer; Array falsch definiert; Jahr 2000 Problematik)
* **Umsetzungsfehler** (von der Spezifikation ins Programm)
* **Oberflächenfehler** (bei GUI oder sonstiger Benutzerinteraktion)

## Testkonzept ([IEEE 829](https://de.wikipedia.org/wiki/Software_Test_Documentation))

![Überblick](./x_gitressourcen/Testen_Überblick.jpg)

### Pflichtenheft / Anforderungsliste
Die hier **definierten Ziele** müssen mit geeigneten Testverfahren am SW-Produkt überprüft werden.  

### Systemdokumentation
Hier sind die **nötigen Details** beschrieben, um die eingesetzten Testverfahren effektiv anwenden zu können.

### Test-Art
Je nach zu überprüfenden Kriterien muss die geeignete Testart gewählt werden. In diesem Modul wollen wir die Funktionalität überprüfen &#8594; **Funktionstest**.

### Test-Spezifikation (Testvorgaben)
Hier wird definiert, wie das Testverfahren **konkret umgesetzt** wird.

### Testdurchführung
Je nach Testmethode (siehe unten) werden unterschiedliche **Drittpersonen** hinzugezogen, um den Test durchzuführen. *Der Programmierer sollte keinen Test selbst ausführen.*

### Abschliessender Testbericht
Ist ein von allen Beteiligten **unterschriebendes Dokument**, das entweder den *Erfolg* (OK), *Misserfolg* (Not OK) der Testdurchführung belegt. Ein **ausgefülltes Testprotokoll** ist beigelegt.
<br>

## Black-Box Testmethode

![Black-Box](./x_gitressourcen/Black-Box Testing.jpg)

Bei dieser Methode werden die Testfälle aus der vorliegenden Spezifikation (z.B. aus Pflichtenheft, Anforderungsliste) oder aber aus der Oberflächenstruktur (z.B. Erfassungsmasken, GUI) hergeleitet. Die innere Struktur des Objektes (Code) wird nicht berücksichtigt und kann unbekannt sein. <br> Es wird vorab ein Testprotokoll erstellt, das geeignete Testfälle in Gruppen beinhaltet. Die Testfälle geben die nötigen Eingaben vor und beschreiben die erwarteten Ausgaben, welche zu überprüfen sind. Das Testprotokoll muss dann von einem Anwender (Drittperson) mit dem zu testenden Objekt durchgeführt werden. 

## White-Box Testmethode

![White-Box](./x_gitressourcen/White-Box Testing.jpg)

Bei der White-Box-Methode werden die Testfälle mit Kenntnis der internen Strukturen des Testobjekts (Source-Code) entwickelt, d.h. diese Methode kann nur von Entwicklern eingesetzt werden. Zu den in der Praxis einsetzbaren White-Box-Methoden zählen **Review**, **Walkthrough**, **Debugging**. Der **laufende Test beim Programmieren** ist ebenfalls ein White-Box-Test (&#8594;J-Unit). <br> Die meisten Entwicklungsumgebungen stellen dazu umfangreiche Tools zur Verfügung (Debugger mit Test-Suites), es sind aber auch eigenständige Applikationen für White-Box-Tests auf dem Markt erhältlich. (Jenkins)

---

# Code-Review

Wir wollen uns in diesem Modul mit den [Code-Reviews](https://de.wikipedia.org/wiki/Review_(Softwaretest)) beschäftigen, welche standartisiert sind ([IEEE 1028](https://de.wikipedia.org/wiki/Software_Reviews_and_Audits)). 

Beim Code-Review wird ein Programm nach oder während der Entwicklung von einem oder mehreren Gutachtern Korrektur gelesen, um mögliche Fehler, Vereinfachungen oder Testfälle zu finden. Dabei kann der Gutachter selbst ein Softwareentwickler sein. 

**Vorteile** des Code-Review: 

* Für unerfahrene Entwickler bietet der Code-Review durch einen erfahrenen Programmierer (Experte) eine gute Möglichkeit, sich schnell und **praxisorientiert weiterzubilden**. <br> 
* Der Einsatz von Reviews führt zu einer **deutlichen Reduktion von Fehlern**. <br>
* Resultate von Code-Reviews sind neben den damit **gefundenen Fehlern** eine **verbesserte Codequalität**. <br>
* Reviews und Inspections können somit die Softwareentstehungskosten um bis zu **30% reduzieren**.


**Anforderungen** an den Code-Review:

* Definition von klaren Zielen des Reviews (Vorbereitung / Checkliste) <br>
* Auswahl der geeigneten Reviewtechnik (Technisches, formelles, informelles Review, Walkthrough, Inspektion) <br>
* Konstruktive Kritikfähigkeit: gefundene Fehler werden objektiv zur Sprache gebracht und positiv aufgenommen. <br>
* Existenz einer Kultur von Lernen und Prozessverbesserung <br>
* Der Experte (Begutachter) darf den Code nicht selbst geschrieben haben.

**Review-Technik: Stellungnahmeverfahren**

Bei unserer Review-Technik bespricht der Autor seine Arbeitsergebnis mit dem Gutachter (Experte / Lehrperson) zur Beurteilung.

---

![ToDo](../x_gitressourcen/ToDo.png) Do To:

Laden Sie die [Vorlage](./Review_Vorlage_m319 V1.0.docx) herunter und studieren Sie sie.

* Erkennen Sie die oben aufgeführten Abschnitte? <br> 
* Identifizieren Sie die Bereiche im Dokument, welche Sie vor der Review-Sitzung ausfüllen müssen, 
* und jene Bereiche, die der Experte während und nach der Sitzung ausfüllen muss. 


> **Hinweis**: Diese Vorlage werden Sie im E3-Camp brauchen!


---

# Checkpoint
* Kenne das Konzept dund dessen Elemente es Testens.
* Weiss den Unterschied zwischen White-Box- und Black-Box-Testmethode.
* Kenne Anforderung und Vorteile der Code-Review-Technik.
* Kann eine Testspezifikation lesen und eine Checkliste für eine Review-Sitzung vorbereiten.


``