![TBZ Logo](../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/m319_picto.jpg)

# Verknüpfte logische Bedingungen 

---

### Lernziel:
* Einfache [Vergleiche](../N1-Flow_Control) können mittels **logischen** Bedingungen verknüpft werden 

---

## Welche logischen Verknüpfungen stehen in JAVA zur Verfügung?

[![Buch](../x_gitressourcen/Buch.jpg)Unterlagen Kap.3.3.3 bis 3.3.5](../Knowledge_Library/Java_Programmieren.pdf)

| Operator | Symbol | Beschreibung |
|:---:|:---:|---|
| = | **==** |  Gleichheit (identisch) |
|  &#8800;| **!=**  |  Verschiedenheit (nicht identisch)|
|  AND |  **&&**, **&** |  Und  |
|  OR | **&#124;&#124;**, **&#124;**  |  Oder; beide auch möglich (ALTgr-7) |
|  NOT |  **!( )** |  Verneinung, Umkehrung, Negation  |
|  XOR| **^** |  Exklusives Oder; beide nicht möglich |
---

[Java ist auch eine Insel: Wenn Sie es genau wissen wollen ...](https://openbook.rheinwerk-verlag.de/javainsel/02_004.html#u2.4.6)

# Checkpoint
* Verstehe den Unterschied zwischen Vergleichen und logischen Operatoren
* Kann verknüpfte logische Bedingungen einsetzen in Selektion und Iteration.


