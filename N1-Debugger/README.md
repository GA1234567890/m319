![TBZ Logo](../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/m319_picto.jpg)

# Testen mit Debugger

---

### Lernziel:
* Was sind Programmfehler
* Welche Programmfehler gibt es?
* Wie kann ich Fehler im Prgramm finden?
* Wie kann ich beim einem Breakpoint die Variableninhalte auslesen?

---

# Etwas Test-Theorie

Um ein qualitiv hochstehendes Programm abliefern zu können, müssen wir es auf **mögliche Fehler** testen. In der Praxis können sehr kleine Fehler sehr grosse **Konsequenzen** haben: (Auto: Scheibenwischerdefekt bei Regen!)

*	Systemabstürze (betrifft auch andere Programme und kommt dem GAU sehr nahe)
*	Programmabbrüche
*	Datenverfälschung &#8594; Anomalien
*	Falsches Systemverhalten
*	Prestigeverlust
*	Frustrierte User/Kunden, usw.

Es ist also wichtig zu ermitteln, wo welche Fehler entstehen können. Abgesehen von Syntaxfehlern und von Laufzeitfehlern gibt es weitere Fehlerarten, die meist schwieriger aufzufinden sind:

* **Syntaxfehler** (= "JAVA Grammatik" falsch geschrieben) <br>
  Werden bei der _Übersetzung eines Programms automatisch_ erkannt  
* **Laufzeitfehler** (z.B. Bedingungen für die Ausführung nicht gegeben &#8594; angegebene Datei fehlt) <br>
  Führen zum _Absturz des Programms_, da der Fehler im Prgramm nicht berücksichtigt wurde!
* Codierfehler (z.B. bei Zeichencodierung, bei Datentypumwandlung)
* Logikfehler (z.B. bei Entscheidungen)
* Strukturfehler (z.B. bei Iterationen)
* Entwurfsfehler (z.B. Realität falsch in der SW abgebildet)
* Datenfehler (z. B. char/integer; Array falsch definiert; Jahr 2000 Problematik)
* Umsetzungsfehler (von der Spezifikation ins Programm)
* Oberflächenfehler (bei GUI oder sonstiger Benutzerinteraktion)

---

# Syntaxfehler finden:
Syntaxfehler werden beim Kompiliervorgang erkannt und angezeigt:

![SyntaxError](./x_gitressourcen/SyntaxErrorView.png)

**Erklärung:**

1. Rote Markierung eines Ausdrucks. &#8594; **Fehlerstelle** &#8592; Kann auch rot unterstrichen sein!
2. **Hinweistext** bei "Mouse-Over"
3. Pulldown mit **Lösungsvorschlägen**
4. Konkrete **Fehlermeldung(en)** imu nteren Bereich (*Build Output* oder *Project Errors*)  ...
5. ... mit Angaben zum genauen **Ort**: (Dateipfad &#8594; Link, Zeile &#8594; 5, Spalte &#8594; 15)
6. **Anzahl** aller Fehler


# Andere Fehler finden:

In der IDE ist ein Tool integriert, das dabei hilft Fehler aufzufinden, indem man den Kontrollfluss und die Datenwerte von Variablen schrittweise überprüfen kann:

## Mit dem Debugger &#8594; ![Debugger](./x_gitressourcen/Debugger.png)


![ToDo](../x_gitressourcen/ToDo.png) Do To: <br>
[IntelliJ Tutorial: Debug my first programm](https://www.jetbrains.com/help/idea/debugging-your-first-java-application.html)

> **Hinweis**: Das Command-Line-Programm "[AverageFinder](./AverageFinder.java)" wird eigentlich in der Konsole ausgeführt, wobei Zahlen angehängt werden. Diese Zahlen werden über die Array-Variable **args** in der Main-Funktion (als Text) gespeichert!

Ausführlicher Überblick:
[IntelliJ Debugger Basics](https://blog.jetbrains.com/idea/2020/05/debugger-basics-in-intellij-idea/)


## Mit Sytem.out.println():

Sie können auch an geeigneten Stellen ein ```...println(...)``` in der Code einfügen, um Varableninhalte oder Infotext auszugeben.

*Nicht vergessen: Die Zeilen müssen wieder entfernt werden vor der Auslieferung!*

---

# Checkpoint
* Wann treten Syntaxfehler auf? Was ist die Folge?
* Wann treten Laufzeitfehler auf? Was ist die Folge?
* In der gewählten IDE können Sie Syntaxfehler erkennen.
* In der gewählten IDE können Sie den Debugger aktivieren und Breakpointssetzen.
* Sie können mit dem Debugger nach einem Breakpoint schrittweise durchs Programm navigieren: Step over, Resume (Continue), Stop
* Sie können ein eigenes Prgramm mit einer Schleife debuggen und dabei bei Breakpoints die Variableninhalte auslesen.



