![TBZ Logo](../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/m319_picto.jpg)

# Komplexe Datentypen

---

### Lernziel:
* Einige nützliche, komplexe Datentypen werden vorgestellt

---

# Statische Felder (Arrays)

![ToDo](../x_gitressourcen/ToDo.png) Do To: **Studieren Sie folgende Kapitel:**

[![Buch](../x_gitressourcen/Buch.jpg)Unterlagen Kap.6](../Knowledge_Library/Java_Programmieren.pdf)

[W3S Arrays](https://www.w3schools.com/java/java_arrays.asp)

[Merkblatt](./m319 Merkblatt statische Felder.pdf)

(Siehe [API](https://docs.oracle.com/en/java/javase/16/docs/api/java.base/java/util/Arrays.html))

*Für Profis:* [Buch Java ist auch eine Insel: Arrays](https://openbook.rheinwerk-verlag.de/javainsel/03_008.html#u3.8)

---

# Zeichenketten / Strings

![ToDo](../x_gitressourcen/ToDo.png) Do To: **Studieren Sie folgende Kapitel:**

[![Buch](../x_gitressourcen/Buch.jpg)Unterlagen Kap.7](../Knowledge_Library/Java_Programmieren.pdf)

![Codierung](./x_gitressourcen/Codierung.png)

(Siehe [API](https://docs.oracle.com/en/java/javase/16/docs/api/java.base/java/lang/String.html))

## Besonderheiten von Strings

[![Buch](../x_gitressourcen/Buch.jpg)Unterlagen Anhang A.19](../Knowledge_Library/Java_Programmieren.pdf)

[W3S Strings](https://www.w3schools.com/java/java_strings.asp)

[W3S String Methods](https://www.w3schools.com/java/java_ref_string.asp)

*Für Profis:* [Buch Java ist auch eine Insel: Strings](https://openbook.rheinwerk-verlag.de/javainsel/04_004.html#u4.4)

---

# Arraylist (dynamische Felder)

![ToDo](../x_gitressourcen/ToDo.png) Do To: **Studieren Sie folgende Kapitel:**
[![Buch](../x_gitressourcen/Buch.jpg)Unterlagen Kap.8.2 bis 8.2.2](../Knowledge_Library/Java_Programmieren.pdf)

[W3S ArrayList](https://www.w3schools.com/java/java_arraylist.asp)

(Siehe [API](https://docs.oracle.com/en/java/javase/16/docs/api/java.base/java/util/ArrayList.html))

## Spezialitäten von dynamischen Arrays

[![Buch](../x_gitressourcen/Buch.jpg)Unterlagen Anhang A.18](../Knowledge_Library/Java_Programmieren.pdf)

*Für Profis:* [Buch Java ist auch eine Insel: ArrayList](https://openbook.rheinwerk-verlag.de/javainsel/16_001.html#u16.1)

---

# Wrapper-Klassen

blabla

![ToDo](../x_gitressourcen/ToDo.png) Do To: 

[W3S Wrapper Klassen](https://www.w3schools.com/java/java_wrapper_classes.asp)

*Für Profis:* [Buch Java ist auch eine Insel: Wrapperklassen](https://openbook.rheinwerk-verlag.de/javainsel/09_004.html#u9.4)

---


# Weitere interessante Datentypen


* **BigInteger**: Ein Big-Integer kann beliebig große ganze Zahlen darstellen. Dabei sind 300-stellige Zahlen keine Seltenheit, aber auch kein Problem für JAVA. (siehe [API](https://docs.oracle.com/en/java/javase/16/docs/api/java.base/java/math/BigInteger.html))

* **BigDecimal**: Dagegen kann ein Big-Decimal mit beliebig vielen Nachkommastellen umgehen. Die Zahl π auf tausend Nachkommastellen zu berechnen ist mit Java also gut möglich. (siehe [API](https://docs.oracle.com/en/java/javase/16/docs/api/java.base/java/math/BigDecimal.html))


---

# Checkpoint
* Strings können angwendet werden
* Eigenheiten von Strings sind bakannt
* Arrays können angwendet werden (primitive Felder und Arraylist)
* Eigenheiten von Arrays sind bekannt
