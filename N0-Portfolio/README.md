![TBZ Logo](../x_gitressourcen/tbz_logo.png)

# Portfolio
---
### Lernziel:
* Zweck, 
* Wahl und 
* Installation eines **persönlichen, digitalen Portfolios**

-- 

## Was ist eine Portfolio?
![ePortfolio](./x_gitressourcen/eportfolioWords.jpg)

Der Begriff Portfolio wird vielfälltig benutzt:

* Sammlung von (eigenen) Kunstwerken
* Sammelmappe mit Bewerbungsunterlagen
* Sammlung von Wertpapieren (z.B. Aktien)
* Sammlung aller Produkte einer (Hersteller-) Firma
* Sammlung von Belegen und Dokumenten
* Art von Journal: **Sammlung von Lern-Ressourcen und -Produkten**

Wir wollen ein digitales Tool verwenden, um ein Lern-Portfolio zu führen! Vorzugsweise entscheiden Sie sich für ein Tool das sie bereits kennen, favorisieren oder durch die Lehrfirma vorgegeben ist (LMS). 

Das Tool sollte es ermöglichen, einzelne Bereiche (=Modul,Gruppe/Einzel) zu unterhalten und zu jedem Bereich ein Link erstellen zu können mit Schreibrechten. Diesen Link müssen Sie **für ihre Lehrperson freigeben und überreichen**.


## Wozu?

Beim SOL werden Sie viele Themen, Anwendungen und Erfahrungen kennenlernen und entwickeln. Diese "Lernelemente" gehen verloren, wenn sie diese nicht "verarbeiten". (Beim *reinen Lesen* hat das Kurzzeitgedächnis eine Latenzzeit von wenigen Minuten)

Durchs geeignete Übertragen in ihr Portfolio "verarbeiten" Sie die Lernelemente und übertragen Sie ins Langzeitgedächnis. "Später" (im Berufsleben) können Sie ihr Portfolio wieder durchkämmen, die Lernelemente nachlesen und durch Repetition auffrischen.

Die beste Wirkung hat ein Portfolio, wenn sie die Lernelemente vernetzen und auch im Berufsalltag weiterentwickeln.

---

![ToDo](../x_gitressourcen/ToDo.png) Do To:

# Wahl eines Tools:

## OneNote

![OneNote](./x_gitressourcen/OneNote.jpg)

Ist im TBZ-Officepaket enthalten. Transfer auf andere Konten möglich. Pro Modul ein Notizbuch eröffnen.

**Anm**.: Bitte nicht über Bildschirmrand hinaus Elemente platzieren ...

![Video:](../x_gitressourcen/Video.png) 12min 
[![Tutorial](https://img.youtube.com/vi/Qc-2OUCbvO0/0.jpg)](https://www.youtube.com/watch?v=Qc-2OUCbvO0)


## Evernote

![Evernote](./x_gitressourcen/EverNote.jpg)

Der Pionieer und Platzhirsch bei den Notizbücher. Gratis sollte reichen für TBZ-Gebrauch. ./x_gitressourcen/OneNote.jpg

![Video:](../x_gitressourcen/Video.png) 12min
[![Tutorial](https://img.youtube.com/vi/iOE-XiX95cY/0.jpg)](https://www.youtube.com/watch?v=iOE-XiX95cY)

## Confluence (LMS)

![Confluence](./x_gitressourcen/Confluence.png)

Confluence ist eine Wiki Software für die Zusammenarbeit im Unternehmen. Vorwiegend wird Confluence für die Kollaboration und das Wissensmanagement innerhalb eines Unternehmens genutzt. Zusätzlich bietet Confluence durch seine Flexibilität viele weitere Einsatzbereiche. Dazu gehören zum Beispiel die Erstellung von Intranetseiten oder QM Systeme auf Basis von Confluence. Ebenso bietet Confluence umfangreiche Integrationsmöglichkeiten zu anderen Atlassian Produkten wie Jira, Fisheye, Clover, Crucible, Bamboo oder Crowd. 

## GitLAB/HUB

Ihr GitLAB/HUB kann auch als Portfolio benutzt werden. Macht vor allem bei den API-Lernenden Sinn. Falls sie Markdown kenne und bereits Umgang mit einem GIT-System haben

Anm: Sollte lokal bearbeitet und zur Abgabe von Belegen auf den Server übertragen werden ("add-commit-push"). 

Siehe auch:
[Portfolio](../N0-GitLAB/readme.md#Verwendung als Portfolio Tool)

## Miro

Das in diesem Modul bereits kennegelernte Tool lässt sich bestens für ein Portfolio verwenden. Vorteil ist die 

Pro Modul ein Board verwenden. Share with Comment/Edit mit Passwort.

![Video:](../x_gitressourcen/Video.png)
[![](https://img.youtube.com/vi/eqPz4gIKJNQ/0.jpg)](https://www.youtube.com/watch?v=eqPz4gIKJNQ)

**Anm**.: Mit dem EDU Account (&#8594; beantragen) können sie die Lehrperson auch als Mitglied einladen.

## Weitere

Sie können ein beliebig anderes Tool verwenden, wenn sie Bereiche freigeben können ...

## LMS (Learning Management System)

Falls sie in der Firma ein LMs verwenden und Bereiche mit Schreibrechten an die Leehrperson weitergeben können, ist das eine gute Wahl.

---
![ToDo](../x_gitressourcen/ToDo.png) Do To:

# Link für Tandem-Partner und LP mit Schreibrechten

1. Erstellen Sie mit ihrem Tandem-Partner einen Link auf die **gemeinsamer Bereich** dieses Moduls (Notebook, Bereich, ...) und versehen Sie den Bereich mit Schreibrechten für ihren Tendem-Partner und die Lehrperson.

2. Übertragen Sie den Link ins Miroboard im **Team Control Center** in ihrem Team unter "Enter Link to Group-Portfolio" mit CTRL-K:<br>![GroupLink](./x_gitressourcen/LinkGroup.png)<br> Falls sie keinen Zugang zum Link finden, senden Sie ein Einladugs-Email an die Lerhperson. Sie wird den Link dann platzieren.

3. Erstellen Sie für sich einen Link auf die **Bereich für Einzelarbeit** dieses Moduls (Notebook, Bereich, ...) und versehen Sie den Bereich mit Schreibrechten für die Lehrperson.

4. Übertragen Sie den Link ins Miroboard im **Team Control Center** in ihrem Team unter "Enter Link to Portfolio Student X" mit CTRL-K:<br>![GroupLink](./x_gitressourcen/LinkSingle.png)<br> Falls sie keinen Zugang zum Link finden, senden Sie ein Einladugs-Email an die Lerhperson. Sie wird den Link dann platzieren.

---

# Checkpoint
* Ich weiss wozu ein Portfolio für den Lernprozess ist und wie man damit umgeht.
* Habe ein Portfolio Tool ausgewählt.
* Habe zwei Bereiche freigegeben (Group / Student) und im Miroboard eingegeben (oder Einladung an LP gesendet). <br> Benennen Sie die Bereiche/Links mit **Klasse-Modul-Name(n)**
* Melden Sie die Fertigstellung und erwarten Sie eine Bestätigung der Lehrperson, ob der Zugriff funktioniert.
* **LERNPROZESS**: "Ich werde mir beim Erstellen eines jeden neuen Lernelementes überlegen, wie ich es *formulatiere*, *niederschreibe*, *kommentiere* und evtl. *vernetze*." (Nicht COPY-PASTE)


