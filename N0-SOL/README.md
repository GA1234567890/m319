![TBZ Logo](../x_gitressourcen/tbz_logo.png)

# Lernenden-zentriertes Lernen: SOL

---
### Lernziele:
* Wozu SOL im Unterricht?
* Welche Rollen gibt es und was sind Ressourcen?
* Einführung in die Begriffe, die beim SOL relevant sind.

---

**Lehrerzentriertes Lernen**: Ist ein Führungsstil und eine Form des Unterrichtens, bei denen alle wesentlichen Impulse, Aktionen und Entscheidungen von dem Lehrer oder der Lehrerin ausgehen: (Didaktisches Dreieck im Schulzimmer)

![SOL](./x_gitressourcen/LZL.jpg)


Wenn Lernende bei vorgegebenen Inhalten und Zielen ihr eigenes Lernen selbst steuern und Entscheidungen über die Art und Weise ihrer Lernorganisation fällen, so spricht man vom **selbstorganisierten Lernen**, sprich "**SOL**".

![SOL](./x_gitressourcen/SOL.jpg)
Innerhalb des vorgegebenen Rahmens ist individuelles Lernen möglich


## Wozu?

Die Anforderung der Wirtschaft für neue Berufsleute haben sich gegenüber der Vergangenheit geändert. Die Lehr- und Lernformen müssen sich auch anpassen und neben den **Fachkompetenzen** (Wissen etc.) müssen auch **überfachliche Kompetenzen** miteinbezogen und trainiert werden.


## Rollen

* Der Lernende wird zum "**Entdecker**", der seinen eigenen **Lernprozess** steuert. Der Lernenden arbeitet in einem **Tandem-Team**, d.h. jede(r) hat einen Partner(in).
* Die Lehrperson wird zum "**Coach**", die den Lernprozess begleitet.
* Die Lehrperson wird zum "**Experten**", die **Kompetenzen** abnimmt.

## Ressourcen (Quellen)
* Die **zentrale Plattform** (Miro) dient als Drehscheibe für Kommunikation, Planung, Austausch.
*Die Ziele sind in einem **Kompetenzraster** formuliert und indiziert.
* **Lern-Ressourcen** dazu (Inputs, Unterlagen, Übungen, etc) sind onlne (GitLAB, MS-Stream, YT, etc.) verfügbar und referenziert.
* Das **Vorwissen** des Lernenden.
* Das Wissen der Klasse soll in **Kooperation** genutzt werden können.


---

# Begriffe:

## Kompetenz:
Gemeint ist die Fähigkeit und Fertigkeit, in den genannten Gebieten Probleme zu lösen, sowie die Bereitschaft, dies auch zu tun.
Im Allgemeinen sind sachlich-kategoriale, methodische und selbststeuernde Elemente verknüpft, einschließlich ihrer Anwendung auf ganz unterschiedliche Gegenstände.
-> Umgangssprachlich: „Jemand ist kompetent“ 

 KURZ:  **= Wissen + Können + Anwenden**

![Kompetenzen](./x_gitressourcen/Kompetenzen.jpg)

### Handlungskompetenz:
„Handlungskompetenz wird verstanden als die Bereitschaft und Befähigung des Einzelnen, sich in beruflichen, gesellschaftlichen und privaten Situationen sachgerecht durchdacht sowie individuell und sozial verantwortlich zu verhalten.“ 
Beinhaltet Lern- und Methodenkompetenz, die wiederum als Voraussetzung Fach-, Sozial- und Selbstkompetenz haben!

### Handlungsziel
Messbaren Beschreibung eines Zustandes und/oder Produkts.
Zur Erreichung dieses Ziels müssen aktive Handlungen ausgeführt werden, die durch verschiedenen Kompetenzbereiche charakterisiert werden können.

Handlungsziele werden oft beschreibend formuliert: „Ich kann ...“, „Ich habe ...angewandt“, „Ich weiss ... „

### Kompetenzraster
Ein Raster mit in Kompetenzfelder eingeteilte Handlungsziele. 

![](./x_gitressourcen/KR.png)

**Zweck des Kompetenzrasters:**

* Zielformulierung für selbstorganisiertes Lernen
* Inhaltsbestimmung für Lernsequenzen
* Orientierungshilfe zur Lernstrategieentwicklung (Offene Fragen)
* Vorgaben zur Lernzielkontrolle 

Einteilung in Fachgebiet, Themenbereiche, Schwierigkeitsgrad, Aufwand, Zeitbedarf, Wissenstand, etc. möglich.

Jedes erfolgreich abgeschlossene Kompetenzfeld ergibt aufsummierte Punkte, um am Ende eine abschliessende Note zu evaluieren.


### Portfolio

Ist eine Sammlung aller "**Daten und Informationen**", die Aufgrund des Lernprozesses entstanden (**Urheberrecht**!) oder relevant sind. (= elektronische Sammelmappe)

![Portfolio](./x_gitressourcen/Portfolio.jpg)

In der Praxis wird ein "**persönliches**", digitales Tool verwendet: OneNote, EverNote, confluence, Miro, GitLAB, ... oder ein LMS. Die Lerhrperson erhält einen Link mit Schreibrechten.


### Kompetenznachweis und Beleg
1. Handlung, bei der eine Lernzielkontrolle durchgeführt wird, um die geforderten Handlungskompetenzen nachzuweisen. 
&#8594; Fachgespräch, Demonstration, Präsentation, Cast anhand eines "**Belegs**" (&#8594; Datei)!
2. Bestätigung, dass die geforderte Handlungskompetenzen ausgewiesen ist! D.h. der Beleg ist im Portfolio abgelegt und im **Punkteraster** als **PASS** markiert!


![Belege](./x_gitressourcen/Belege.jpg)

---


&#8594; [Learning Rules](./SOL_Learning_Rules.md)

---

# Checkpoint
* Was ist ein **Tandem**?
* Aus welchen Kompetenzen bildet sich die **Handlungskompetenz**?
* Was ist ein **Beleg** und wozu wird er verwendet?
* Wann ist ein **Nachweis** abgenommen?
* Welche **Regeln** gelten im Schulzimmer?







