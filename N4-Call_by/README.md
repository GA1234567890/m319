![TBZ Logo](../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/m319_picto.jpg)

# Abstraktionen mit Parameter


---

### Lernziel:
* Zweck, 
*

---

![Erklärung](../x_gitressourcen/UC.jpg)

---

![ToDo](../x_gitressourcen/ToDo.png) Do To:

# Titel:

babla

#Funktionen mit Parameter

[![Buch](../x_gitressourcen/Buch.jpg)Unterlagen Kap.5](../Knowledge_Library/Java_Programmieren.pdf)


## Untertitel

blabla

![OneNote](./x_gitressourcen/OneNote.jpg)

Ist im TBZ-Officepaket enthalten. Transfer auf andere Konten möglich. Pro Modul ein Notizbuch eröffnen.

**Anm**.: Bitte nicht über Bildschirmrand hinaus Elemente platzieren ...

![Video:](../x_gitressourcen/Video.png)
[![Tutorial](https://img.youtube.com/vi/Qc-2OUCbvO0/0.jpg)](https://www.youtube.com/watch?v=Qc-2OUCbvO0)


---

# Checkpoint
* I
* 
* **LERNPROZESS**: "Ich ." (Nicht COPY-PASTE)


