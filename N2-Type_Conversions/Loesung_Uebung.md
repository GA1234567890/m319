![TBZ Logo](../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/m319_picto.jpg)


# Lösung der Übung
 
```java
import static ch.tbz.lib.Input.*;    // Lib for input functions

public class TestKonv {
    public static void main(String[] args) {
        double d;
        do {
            d = inputDouble("Enter a test number:");

            float f = (float) d;    // >>> explicit conversion to float
            int i   =   (int) d;    // >>> explicit conversion to int
            byte b  =  (byte) d;    // >>> explicit conversion to byte
            char c  =  (char) d;    // >>> explicit conversion to char
            
            System.out.println("Double value " + d);
            System.out.println("Float  value " + f);
            System.out.println("Int    value " + i);
            System.out.println("Byte   value " + b);
            System.out.println("Char   value \'" + c + "\'\n\n");
        } while( d != 0 );
    }
}
```

**1. Ergebnisse:**

| Nr. | Eingabe | Ausgabe / Ergebnis | Hinweis |
|:---:|:-------:|-----------|--------|
| 1. | 127 | ok | '127' überall speicherbar |
| 2. | 128 | Byte: -128 | Byte B-1 Komplement (* )  '**1**0000000' = -128  |
| 3. | 255 (1 Byte) | Byte: -1 | Byte B-1 Komplement '**1**1111111' = -1  |
| 4. | 256 (2 Byte) | Byte: 0 | Byte Overflow '*00000001* 0**0000000**' = 0 |
| 5. | 0.1234567890123456789 | int: Nachkommastellen werden abgeschnitten<br> float: Genauigkeit nimmt ab | Double: nur 18 Stellen nach dem Komma |
| 6. | 1.234567890123456789 |  float: Genauigkeit nimmt ab | Double: nur 17 Stellen |
| 7. | 1234567890.123456789 | float: ungenaues Abschneiden, Exponentialdarstellung | Double: 17 Stellen mit Exponentiladarstellung|
| 8. | 1234567890123456789 | float: ungenaues Abschneiden, Exponentialdarstellung | Int Overflow! |
| 9. | 1**e**2345 | float und double: Overflow mit ***Infinity*** | kein Laufzeitfehler hier!  |


Bei einer Bereichsüberschreitung (Overflow) werden im Speicher die *überzähligen Bytes* weggelassen und der **übriggebliebene Rest** im Speicher (falsch) als Zahl interpretiert. (Wie in Punkt 4 dargestellt!)

Bereichsüberschreitung bei float und double ergeben den Wert **Infinity**. Nachfolgende Berechnungen werden entsprechend *falsch* berechnet. Siehe auch **NaN**:
[Buch Java ist auch eine Insel](https://openbook.rheinwerk-verlag.de/javainsel/22_002.html#i22_34)

**) B-1 Komplement: Siehe Binärzahlen m162*

--

**2. Frage:**
Löschen Sie einen Cast-Operator im obigen Quellcode. Was geschieht?

--> Es escheint eine Kompilerfehler: **incompatible types: possible lossy conversion from double to int: 12**

