![TBZ Logo](../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/m319_picto.jpg)

# Typumwandlung von primitiven Datentypen

---

### Lernziel:
* Weshalb ist eine Umwandlung des Datentyps nötig?
* Wie wird der Datentyp angepasst?
* Worauf muss man achten? 

---

# Typenkonvertierung in Java

Werte in einer Variable werden durch den Wertebereich des verwendeten (primitiven) Datentyps begrenzt, so z.B. kann in einer `Byte`-Variable nur eine Zahl zwischen `-128` und `+128` abgelegt werden.


## Erweiternde Konvertierung

Will man den Inhalt über die Grenze vergrössern oder bezgl. Nachkommastellen erweitern können, muss der Inhalt in eine Variable mit grösserem Wertebereich (`byte` in z.B. `int` oder `float`) *umgespeichert* werden. 

Offensichtlich ergibt sich dabei kein Nachteil - ausser dass mehr Speicherplatz benutzt wird - und diese erweiternde Konvertierung wird bei der Zuweisung in den grösseren Datentyp **automatisch** (implizit) vollzogen:


![Widening conversion](./x_gitressourcen/TypeConversion.png)

```java
class Low2High {
   public static void main(String[] args) {
      byte b = 100;
      int  i = b;  // >>> implicit conversion to int
      long l = i;  // >>> implicit conversion to long
      System.out.println("Byte value " +b);
      System.out.println("Int  value " +i);
      System.out.println("Long value " +l);
   }
} 
```

## Verengende Konvertierung

Will man hingegen den Wertebereich oder z.B. die Nachkommastellen einschränken, wird ein kleinerer Datentyp gewählt. Diese gewünschte Konvertierung(-srichtung) muss dem Kompiler mit einem sog. Cast-Operator **(Typ)** mitgeteilt werden: <br> `zielVar =(zielTyp)urVar;`. Sonst gibt er beim Kompilieren eine Fehlermeldung aus!


```java
class High2Low {
   public static void main(String[] args) {
      double d = 100.05;
      long   l = (long)d;  // >>> explicit conversion to long
      int    i = (int)l;   // >>> explicit conversion to int
      System.out.println("Double value " +d);
      System.out.println("Long value " +l);
      System.out.println("Int value " +i);
   }
} 
```

### Konsequenzen:

Eine Umwandlung von einer "grösseren" in eine "kleinere" Variable ist problematisch, da **der Inhalt erst zur Laufzeit bestimmt ist**:

*	Wenn der konkrete Inhalt der grösseren Variable den Wertebereich der kleineren Variable überschreitet, gibt es einen *meist unerkannten* **Konvertierungsfehler**! (Siehe Übung unten)

*	**Ausnahme**: Wenn nur der Nachkommabereich zu gross ist, wird dieser kurzerhand abgeschnitten. Es erfolgt also *evtl.* ein **Datenverlust**! 

![Umwandlung](./x_gitressourcen/Umwandlung.png)

*	Spezielle Datentypen (boolean / char) lassen sich nur beschränkt sinnvoll konvertieren!

> **Wichtig**: Eine verengende Konvertierung sollte gut überlegt sein und im Code überprüft werden (Range check).

## Übersicht

![Übersicht](./x_gitressourcen/java_data_type_conversion_chart.jpg)

---

# Übungen
 
![ToDo](../x_gitressourcen/ToDo.png) Do To:

Übernehmen Sie folgendes Programm in ihre IDE: [TestKonv.java](./TestKonv.java)

```java
import static ch.tbz.lib.Input.*;    // Lib for input functions

public class TestKonv {
    public static void main(String[] args) {
        double d;
        do {
            d = inputDouble("Enter a test number:");

            float f = (float) d;    // >>> explicit conversion to float
            int i   =   (int) d;    // >>> explicit conversion to int
            byte b  =  (byte) d;    // >>> explicit conversion to byte
            char c  =  (char) d;    // >>> explicit conversion to char
            
            System.out.println("Double value " + d);
            System.out.println("Float  value " + f);
            System.out.println("Int    value " + i);
            System.out.println("Byte   value " + b);
            System.out.println("Char   value \'" + c + "\'\n\n");
        } while( d != 0 );
    }
}
```

**1. Geben Sie folgende Testzahlen ein und kommentieren Sie das Ergebnis:**
(Beachten Sie dabei die Grösse des Typs im Speicher! Gibt's Fehlermeldungen?)

1. 127
2. 128
3. 255
4. 256
5. 0.1234567890123456789
6. 1.234567890123456789
7. 1234567890.123456789
8. 1234567890123456789
9. 1e2345


**2. Frage:** Löschen Sie einen Cast-Operator im obigen Quellcode. Was geschieht?

**3. Praxis:**
[W3S Casting](https://www.w3schools.com/java/java_type_casting.asp)


---

# Checkpoint
* Welche zwei Richtungen gibt es bei einer Daten-Konvertierung und in welcher **Reihenfolge** stehen die Datentypen?
* Was ist ein **Cast-Operator** und wie wird er gesetzt?
* **Wozu** braucht es einen Cast-Operator?
* Wo liegt die **Problematik** bei der verengenden Konvertierung?

[(Lösung Übungen)](./Loesung_Uebung.md)

