![TBZ Logo](../../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/Fachkompetenz.png)

# Kompetenznachweis E1


# Debug-Auftrag: (Gruppenarbeit)

Ein Debugger kann zur Einzelschrittabarbeitung und zur Variablenüberwachung eingesetzt werden.
Breakpoints können sinnvoll gesetzt werden. (z.B. in Iteration).


1. Holen Sie bei der Lehrperson den Debug-Auftrag (Serie - Task). 
2. Platzieren Sie die entsprechende Planer-Karte vom KN-Feld in ihren Planer "**In progress**".
3. Setzen Sie als Team den Auftrag mit der IDE um und dokumentiren Sie die Resultate.
4. Platzieren Sie die entsprechende Planer-Karte im Planer auf "**To examine**".
5. Informieren Sie die LP und demonstrieren Sie ihr Antworten.
6. Die Lehrperson platziert die entsprechende Planer-Karte im Planer auf "**Passed**" oder "**Redo**" --> 3.


## Anforderungen

* Live Demonstration der Aufträge 


