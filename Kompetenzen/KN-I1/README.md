![TBZ Logo](../../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/Fachkompetenz.png)

# Kompetenznachweis I1


# Auftrag: (Gruppenarbeit)

Aufbau und Syntax eines Programms sind bekannt. Selektion und Iterationen (fuss- und kopfgesteuert) können programmiert werden.
Mathematische Operationen (+ - x / % ++ -- += -= *= /= %=) können programmiert werden.

1. Holen Sie bei der Lehrperson den Auftrag (Serie - Task). 
2. Platzieren Sie die entsprechende Planer-Karte vom KN-Feld I1 in ihren Planer "**In progress**".
3. Setzen Sie als Team den Auftrag in *einem* Java-File um.
4. Platzieren Sie die entsprechende Planer-Karte im Planer auf "**To examine**".
5. Informieren Sie die LP und demonstrieren Sie ihr Lösungs-Programm.
6. Die Lehrperson platziert die entsprechende Planer-Karte im Planer auf "**Passed**" oder "**Redo**" --> 3.

 

## Anforderungen

* **Einfaches Programm** ...
* mit **Eingabemöglichkeit** der nötigen, variablen Daten ("Bitte Seite a eingeben:" x.y), ...
* einem **Berechnungsteil** mit abgespeicherten Resultaten (float resultat := ...) ) und ...
* **Ausgaben** der Resultate, wobei deren Bedeutung angegeben ist. ("Die Fläche ist: 1.21 m^2")


