![TBZ Logo](../../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/Fachkompetenz.png)

# Kompetenznachweis D4


# Projektanforderung Datentyp und Parameterübergabe: (Einzelarbeit)

Für eigene Aufgabenstellungen werden komplexe Datentypen deklariert und initialisiert.

Konzept der Parameterübergabe (call-by-refererence, call-by-value) können angewendet werden.

## Anforderungen

* Niveau 4, d.h. alle Anforderungen von D1 bis D3 sind gesteigert
* In Kombination mit A4, I4 umzusetzen
* Integration der Anforderungen D4 in die I4-Projekte, oder zusätzlich eigene Programme (Niveau 4) erstellt. 




