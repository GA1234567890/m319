![TBZ Logo](../../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/Fachkompetenz.png)

# Kompetenznachweis I2


# Auftrag: (Gruppenarbeit)

Ein detailliert vorgegebener Ablauf kann programmiert werden. Aufruf von Funktionen sind bekannt und können eingesetzt werden.

Bedingungen werden mit logischen Verknüpfungen (AND, OR, NOT) implementiert.

Zeilen- und Abschnitts-Kommentar kann eingesetzt werden.

1. Holen Sie bei der Lehrperson den Auftrag (Serie - Task). 
2. Platzieren Sie die entsprechende Planer-Karte vom KN-Feld I2 in ihren Planer "**In progress**".
3. Setzen Sie als Team den Auftrag in *einem* Java-File um.
4. Platzieren Sie die entsprechende Planer-Karte im Planer auf "**To examine**".
5. Informieren Sie die LP und demonstrieren Sie ihr Lösungs-Programm.
6. Die Lehrperson platziert die entsprechende Planer-Karte im Planer auf "**Passed**" oder "**Redo**" --> 3.


## Anforderungen

* Niveau 2, d.h. alle Anforderungen von I1 sind gesteigert.
* **Kommentar** in Zeilen und über Abschnitten.
* Bedingungen sind mit **logischen Verknüpfungen** formuliert.
* Wo sinnvoll werden **Funktionsaufrufe** benutzt. 

