![TBZ Logo](../../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/Fachkompetenz.png)

# Kompetenznachweis I4


# Projekte: (Einzelarbeit)

Die eigenen Programme verwenden Standardmethoden zur Bearbeitung von komplexen Datentypen.
Die Programme widerspiegeln die Anforderungen und zeigen ein fundiertes Wissen wie Probleme in Strukturen unterteilt (mit Methoden) werden.

1. Platzieren Sie die *kopierte* Planer-Karte vom KN-Feld I4 in ihren Planer "**In progress**".
2. Erstellen Sie eine **Anforderungsliste *IN* der Planer-Karte** und besprechen Sie mit der Lehrperson ihr Vorhaben anhand ihrer Anforderungsliste. Das Programm muss Niveau-gerecht sein. ![Planercard](../x_gitressourcen/Planer Card Own Task.png)
3. Setzen Sie den Auftrag in *einem* Java-File um. (Einzelarbeit)
4. Platzieren Sie die entsprechende Planer-Karte im Planer auf "**To examine**". <br> Ein Link zum Programmquellcode kann in der Planerkarte gesetzt werden.
5. Informieren Sie die LP und demonstrieren Sie ihr Lösungs-Programm.
6. Die Lehrperson platziert die entsprechende Planer-Karte im Planer auf "**Passed**" oder "**Redo**" --> 3.


## Anforderungen

* Niveau 4, d.h. alle Anforderungen von I1 bis I3 sind gesteigert
* Formulierte Aufträge (A4) robust umsetzen.
* ...
* Wenn möglich Anwendungsfälle (Use Cases) umgesetzt (I4) die dann überprüft (E4) werden sollen.



