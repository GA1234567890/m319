![TBZ Logo](../../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/Fachkompetenz.png)

# Kompetenznachweis D3


# Auftrag: (Gruppenarbeit)

Methoden der zusammengesetzten Datentypen sind bekannt und können verwendet werden (z.B. toString). Kann zusammengesetzte Datentypen initialisieren und Zuweisungen vornehmen. (z.B. Array, String).

## Anforderungen

* Niveau 3, d.h. alle Anforderungen von D1 bis D2 sind gesteigert
* ...



