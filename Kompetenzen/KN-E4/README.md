![TBZ Logo](../../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/Fachkompetenz.png)

# Kompetenznachweis E4


# Projektabnahme: (Einzelarbeit)

Die eigenen Programme werden einer Qualitätsüberprüfung unterzogen mittels durchgeführtem und protokolliertem Code-Review.

Exemplarische Testfälle werden gemäss den Use Cases aufgezeigt. (“Happy Path”)

## Anforderungen

* Niveau 4, d.h. alle Anforderungen von E3 sind gesteigert
* Abnahme der Projekte von I4
* Wenn vorhanden Testfälle anhand der beschriebenen Use Cases von A4 überprüfen
* ...





