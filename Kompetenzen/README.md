![TBZ Logo](../x_gitressourcen/tbz_logo.png)
![m319 Picto](./x_gitressourcen/Fachkompetenz.png)

# Fachkompetenzen und Nachweise


## Kompetenzraster
Das deutsche [Kompetenzraster](./LB Kompetenzraster 319 de.pdf) (KR)
gibt Auskunft über alle abzulegenden Kompetenzfelder und über die Modalität. Anschliessend an das 

The english [competency grid](./LB Kompetenzraster 319 en.pdf) (KR)
lists only the grid.

## Nachweise

Benutzen Sie die **Miro Lerning Map**, um die Nachweise anzumelden, bzw. abzuholen!