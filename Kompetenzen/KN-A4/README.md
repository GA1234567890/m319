![TBZ Logo](../../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/Fachkompetenz.png)

# Kompetenznachweis A4


# Projektaufträge erstellen: (Einzelarbeit)

Für eigene Aufgabenstellungen werden Anforderungen formuliert (z.B. auch als Use Cases) und entsprechend grafisch dargestellt. (UML AD und evtl. UML Use Case)

## Anforderungen

* Niveau 4, d.h. alle Anforderungen von A1 bis A3 sind gesteigert
* ...
* Wenn möglich Anwendungsfälle (Use Cases) beschreiben, die umgesetzt (I4) und überprüft (E4) werden sollen.






