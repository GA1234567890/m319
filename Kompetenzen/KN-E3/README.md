![TBZ Logo](../../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/Fachkompetenz.png)

# Kompetenznachweis E3


# Umsetzung und Review: (Einzelarbeit)

Einsatz von Code-Formatierung (z.B. FIXME, TODO, etc.). Programme sind kommentiert und formatiert.

Code-Review zum I3-Programm durchgeführt und protokolliert. 
Laden Sie die [Vorlage](../../N2-Testing/Review_Vorlage_m319 V1.0.docx) herunter und benutzen Sie sie!.

## Anforderungen

* Umsetzung mit/im Projekt von I3 erfolgt
* 






