![TBZ Logo](../x_gitressourcen/tbz_logo.png)
![Library](./x_gitressourcen/Library.jpg)

# Knowlege Library

Hier finden Sie alle referenzierten, allgemeinen Unterlagen und mehr ...

**Viel Vergnügen beim Lernen!**

## PDFs:
* [![Buch](../x_gitressourcen/Buch.jpg)Unterlagen](./Java_Programmieren.pdf) <br>
  [Programmieraufgabensammlung zu den Unterlagen](https://www.programmieraufgaben.ch/) <br>
* [Referenz zu Java und AD](./Referenz_Java_UML.pdf)
* [IntelliJIDEA ReferenceCard](IntelliJIDEA_ReferenceCard.pdf)

## TBZ Screencasts
[TBZ Streams: Kanal m319 JAVA](https://web.microsoftstream.com/channel/1f550531-dbe5-47c1-a44f-bdf2f22b6908)

## Poster:
* [Poster Konzept Variablen](../N1-Variables_Constants/ProgBASICS Variablen V1.3.png)
* [Poster Kontrollstrukturen: AD](../N1-Flow_Control/Kontrollstrukturen_AD.png)
* [Poster UML V2.5](./UML 2.5 (Plakat A0).png)
* [Poster Der Softwareentwicklungsprozess](./SW-Entwicklungsprozess.png)

## Links
* [JAVA JDK 16](https://docs.oracle.com/en/java/javase/16/index.html)
* [W3School Java Tutorials](https://www.w3schools.com/java/java_break.asp)
* [Java for Beginners](https://javabeginners.de/index.php) 
* [Java ist eine Insel (ausführliche Doku)](https://openbook.rheinwerk-verlag.de/javainsel/01_001.html#u1)
* [Java advanced tutorials](http://www.java2s.com/)
* [Java Referenz](http://www.java2s.com/ref/java/java.html)
* 
* [Google Java Style Guide](https://google.github.io/styleguide/javaguide.html#s3.3-import-statements)
