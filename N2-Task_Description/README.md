![TBZ Logo](../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/m319_picto.jpg)

# Anforderungen an ein Programm

---

### Lernziel:
* Phasen der Softwareentwicklung 
* Was ist eine Anforderungsliste für SW-Projekte
* Welche Kategorisierung und Strukturierung ist üblich
* Welche Formulierung von Anforderungskriterien ist gewünscht
* Was sind funktionale und was sind nicht-funktionale Anforderungen

---

# Software Entwicklung

Um ein **SW-Produkt** zu erstellen und beim Kunden zu Betreiben durchläuft der Prozess mehrere Phasen:

![Phasen](./x_gitressourcen/SE-Phasen.jpg)

| **Phase**  | **Aktion / Mittel**  | **Zusätzliche Dokumente** |
|:----:|:-----:|-----|
| **Anstoss**  | *Kundenauftrag*  | *Projektdokus eröffnen… (M431, M306)*   |
| **>> Analyse <<**   | *Funktionsmodell, Storyboard,*  | **_Anforderungsliste_** / *Pflichtenheft*  |
| **Entwurf** (Design) | *Struktogramm, UML Diagramme,* *GUI-Design*   | *Funktionsbeschrieb Szenarien Anwendungsfälle (UC)* |
| **Realisation** (Implementation, Programmieren) | *Programmcode (mit Komponententests) Inline-Kommentar* | *Systemdokumentation*  *(JavaDoc)* |
| **Tests** | *Testprotokoll OK/NOK (Integrations- / Systemtests)*  | *Testbeschrieb (Testvorschrift)*  *Code-Review*  |
| **Einführung** (Inbetriebnahme) | *Migration Schulung (M214)* | *Betriebsanleitung Installationsdokumentation*  |
| **Wartung**  | *Helpdesk, Dienstleistung*  | *Servicemanual*  |

In der Analyse-Phase werden die Anforderungen ausgearbeitet.

---

# Was sind Anforderungen?

Bei jedem Computerprogramm geht es darum, bestimmte Erwartungen und Bedürfnisse zu erfüllen. Um diese Erwartungen besser zu beschreiben, stellen wir Anforderungen, die das Programm erfüllen soll.

Beispiel einer einfachen Anforderung: *Ich möchte ein Programm, dass mir den grössten gemeinsamen Teiler (ggt) berechnet.*

Die Anforderung ist genau genug, dass ein Programmierer eine gute Vorstellung hat, was von seinem Programm erwartet wird.

Je mehr Anforderungen beschrieben werden, desto komplexer wird das Programm.

Ein weiteres Beispiel: *Ich möchte ein Programm, mit dem ich rechnen kann, wie ein Taschenrechner.*

Auf den ersten Blick scheint klar zu sein, was der Benutzer möchte. Aber hinter dieser Anforderung verstecken sich Fragen:

Wie genau soll der Taschenrechner rechnen? Nur mit ganzen Zahlen oder auch mit Dezimalzahlen? Und soll der Taschenrechner auch potenzieren und Wurzeln berechnen können? Soll der Taschenrechner seine letzten Berechnungen speichern?

> **Anm**.: Die Anforderungen werden bei IPERKA in den Phasen "I" und "P" zusammengestellt.

## Wie sehen gut formulierte Anforderungen aus?

Somit ist es wichtig, dass Anforderungen möglichst genau formuliert werden. Es geht immer darum die Vorstellungen eines Programms möglichst genau in Sätzen abzubilden (man kann das auch mit Bildern und Diagrammen machen, aber üblicherweise sind Sätze die Basis).

Somit müsste man bei unserem Taschenrechner Beispiel, die Anforderungen in mehrere Beschreibungen auflisten. Etwa so:

-   *Das Programm soll die 4 grundlegenden Operationen durchführen können (+, -,
    \*, /).*
-   *Der Benutzer kann dabei ganze Zahlen oder auch Dezimalzahlen eingeben. Das
    Programm soll mit beiden Zahlen-Arten rechnen können.*
-   *Das Programm soll beim durch Null teilen eine Warnung ausgeben, dass das
    nicht geht.*
-   *Das Programm hat auch eine Operation für die 2er Potenz und für das
    Berechnen von der zweiten Wurzel.*

Das gibt uns schon ein viel genaueres Bild, was das Programm tun soll.

Hier sind ein paar Kriterien für gut formulierte Anforderungen:

**Verständlich** Die Sätze sind verständlich und klar formuliert

**Widerspruchsfrei** In der Beschreibung gibt es keine Widersprüche (dh. nicht in einer Anforderung etwas beschreiben, was in einer anderen Anforderung das Gegenteil ist)

**Vollständig** Alle Anforderungen werden notiert

**Prüfbar** Die Anforderungen müssen durch das Programm überprüft werden können (z.Bsp durch Benutzer-Tests)

>  **Anm**.: Siehe auch [SMART](https://de.wikipedia.org/wiki/SMART_(Projektmanagement))-Formulierung


## Anforderungen klassifizieren

Anforderungen sollen klassifiert werden, indem man deutlich macht, was unbedingt erfüllt werden muss. Man unterscheidet somit zwischen:

**Muss-Anforderungen**: diese sind unverzichtbar und *müssen* erfüllt werden

**Soll-Anforderungen**: diese *können* erfüllt werden, wenn es nicht zu aufwändig wird

**Wunsch-Anforderungen**: *falls* es genügend Zeit (und Geld) hat, werden diese berücksichtigt.


## Wie kann ich Anforderungen strukturieren?

### 1. Liste

Die einfachste Weise, um Anforderungen zu strukturieren, ist sie aufzulisten. Die Liste wird nach der Priorität erstellt. Somit sind Muss-Anforderungen zuoberst auf der Liste. 

> **Anm**.: Die Anforderungsliste ist oft Bestandteil (ein Kapitel) eines *Pflichtenhefts*.


### 2. Anforderungen als Use Case

Die Anforderungen können in sog. **Anwendungsfälle** (Use Cases) strukturiert werden. Dabei geht es darum, jede Anforderung möglichst aus der Sichtweise des Benutzers zu beschreiben. Man nimmt dabei konkrete Beispiele, um einen Ablauf zu beschreiben. 

Ein einfaches Beispiel:

| Use Case   | Summe berechnen |
|:------:|--------------|
| Beschreibung Anwendungsfall | *Der Benutzer gibt zwei ganze Zahlen ein. Danach gibt er das + Symbol ein.*   |
| Resultat  | *Das Programm berechnet die Summe und gibt das Resultat aus.*    |
| Ausnahmen   | *Benutzer gibt falsche Werte ein (z.Bsp. Buchstaben). Das Programm zeigt einen Fehler an.*  |

UML UC: 

![UseCase](./x_gitressourcen/UseCase.jpg)

### 3. Aktivitätsdiagramm

Die Anforderungen können mit **Aktivitätsdiagrammen** dargestellt werdern, was aber heisst, dass wir dann jeden Schritt aus der Sicht des Benutzers zeigen. Somit sind es Use Cases, die im Aktivitätsdiagramm abgebildet werden.

UML AD:

![](./x_gitressourcen/AD_Calc.png)

## Funktionale und nicht-funktionale Anforderungen

Man unterscheidet zwischen funktionalen und nicht-funktionalen Anforderungen:

**Funktionale Anforderungen**: Beschreiben was das Programm tun soll. (Siehe oben!)

**Nicht-funktionale Anforderungen**: Das sind Kriterien wie **Benutzbarkeit**, **Geschwindigkeit** der Programmreaktion und Kriterien, welche die **Qualität** des Programms beschreiben.

---

![ToDo](../x_gitressourcen/ToDo.png) Do To:

1. Laden Sie die [Vorlage](./Anforderungsliste.docx) und das [Beispiel](./Anf_LiftSimulation_V1.2.docx) herunter und studieren Sie diese.

2. **Beschreiben Sie die Anforderung** des folgenden ADs: ![AD_Login](./x_gitressourcen/AD_Login.jpg)

3. **Zeichnen Sie ein AD** zu folgender Anforderung: 

* Einlesen von Daten.
* Wenn Daten gültig sind, dann Daten in Tabelle eintragen, sonst Fehlermeldung ausgeben.
* Wieder neue Daten einlesen. 
* Nach 100 Daten Einlesen beenden und Tabelle als Graphik anzeigen.

([Lösung](./x_gitressourcen/AD_100_Daten.png))


---


## Quellen:

<https://de.wikipedia.org/wiki/Anforderung_(Informatik)>

<https://files.ifi.uzh.ch/rerg/amadeus/teaching/courses/software_engineering_hs07/skript/Kapitel_07.pdf>


---

# Checkpoint
* In welcher **Phase** erstellen wir eine Anforderungsliste?
* Wie **formuliere** ich die Anforderungen?
* Welche **Kategorien** von Kriterien gibts.
* Was sind **funktionale** Anforderungen?
* Was sind **nicht-funktionale** Anforderungen?


