![TBZ Logo](../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/m319_picto.jpg)

# Quellcode Kommentar


### Lernziel:
* Zweck eines Kommentars
* Kommentararten
* Einführung in JavaDoc

---

## Wozu wird ein Kommentar eingesetzt

* Versieht eine Quelltext mit **Metadaten**: Angaben zum Inhalt, Autor, Verion, Änderungshistory, ...
* Kommentiert den Quellcode, um weitere (Hintergrund-)**Informationen** zu geben
* &#8594; Dient der **Orientierung** (beim Einlesen)
* &#8594; Dient der **Wartung**

---

# Überschriften /* ... */ oder //

(1) **Klassen**, **Funktionen** und wichtige **Abschnitte** sollten kommentiert werden:

* Beschreibt den nachfolgenden **Inhalt** (Übersicht)
* Können mehrzeilig ```/* ..*/``` oder auch nur einzeilig ```//```sein.
* Werden **oberhalb** Abschnitt platziert
* Haben dieselbe **Einrückung**, damit die Dastellung der Struktur nicht gestört wird.

Siehe auch (3)!

# Zeilenkommentar // ...

(2) Eine Zeile Code wird kommentiert:

* Beschreibt **WOZU** der Programmcode in der Zeile so programmiert wurde.
* **Verweisst** auf eine Quellen, einen Hintergrund oder einen Ursprung.
* Wird üblicherweise am Ende der Zeile angehängt.


**Schlechtes Beispiel:**

```java
double c = Math.sqrt(Math.pow(a,2) + pow(b,2)); // Wurzel aus a^2 plus b^2
```
Hier wird nur der Code *nochmals* erklärt.

**Gutes Beispiel:**

```java
double c = Math.sqrt(Math.pow(a,2) + pow(b,2)); // Satz von Pythagoras

```
Hier wird der *Ursprung* der Formel belegt.


# JavaDoc /** ... **/

(3) Speziell formatierter Kommentar wird zur System-Dokumentation (API) des Codes verwendet! Das Tool **JavaDoc.exe** kreiert eine HTML Seite aus dem Kommentar.

* Geeingete Überschriften werden im Bezug zum Code gebracht: Klassen, Methoden, Parameter
* Auch unkommentierte Methoden werde in die HTML-Docu eingebunden (Ohne Text)
* **HTML-Tags** können eingebunden werden.

![IntelliJ Tool Menü](./x_gitressourcen/JavaDoc.png)

# Beispielcode

![Comment](./x_gitressourcen/Comment.png)

![ToDo](../x_gitressourcen/ToDo.png) Do To:
[W3S Comments](https://www.w3schools.com/java/java_comments.asp)

## Für Profis: Ausführliches Tutorial

![Video:](../x_gitressourcen/Video.png) 30 min
[![Tutorial](https://img.youtube.com/vi/qLA7HhoajqM/0.jpg)](https://www.youtube.com/watch?v=qLA7HhoajqM)

IntelliJ JavaDoc: >> 27min

---

# Checkpoint
* Kenne die drei Kommentararten
* Weiss wann welche Art einzusetzen ist
* *Für Profis: Kann ein JavaDoc HTML-Seite erstellen*


